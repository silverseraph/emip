% this file is called up by thesis.tex
% content in this file will be fed into the main document

%: ----------------------- name of chapter  -------------------------
\chapter{Results}

Results from from running the program show that the warp was successful, obtained a substantial speed increase over the single threaded implementation, and an even more substantial speed increase on the Xeon Phi.  Performance and accuracy were quantitatively assessed by using 4DCT scans with human placed landmarks.  Additionally, a volume of the same size as was used in \cite{gpu_tps} is provided to be used as a rough timing comparison to an industrial CUDA card.

For the purpose of comparing the difference between different architectures performance for the thin-plate spline, a Voxel Control Point Throughput (VCPT) metric is used.  This is determined by dividing the product of the voxel count and control point count with the runtime of the algorithm.  This is a viable metric for measurement, as the voxel count and control point count adjustment will have a similar effect on all platforms and can be used as a good predictor of how an implementation will scale when given greater numbers of control points or voxels.

Accuracy was not noticeably effected by the differences between the Xeon Phi and the CPU implementation as can be seen in the images below, and in \ref{table:archit_runtime}.  The major points of difference between the Xeon Phi, Xeon multi-threaded, and Xeon single-threaded was mostly in the use of the fastmath package.  The difference, in this instance, caused by using the fastmath package was nearly less than \%1 of the total error value between the target and warped image.

As far as performance of the transform is concerned, the Xeon Phi, at both volume sizes, showed a significant improvement in performance over the CPU implementation.  Given the nature of the MIC architecture, it is unsurprising that the larger volume showed a much greater improvement in speed, as the overhead of organizing the cores is much more significant for smaller data sets than for large ones.

The implementation on the Xeon Phi seems to have much improved performance over the CUDA implementation seen in \cite{gpu_tps}.  Unfortunately, due to hardware unavailability it is impossible to test both on the same system, and the best that can be done is an estimate.  Both are supercomputer-grade hardware, with both being rated at greater than 1Tflops per device.  

\begin{figure} 
    \centering
    \begin{tabular}{l|r|r|r|r}
       Variation                         & Time (s) & MSE      & Size (x, y, z, points) & VCPT \\
       \hline
       Xeon Phi                          & 0.800    & 0.000297 & 512x512x128x139        & 5.830e9 \\
       Xeon w/ AVX Multi-Threaded        & 6.160    & 0.000297 & 512x512x128x139        & 7.571e8 \\
       Xeon w/ AVX Single-Threaded       & 24.931   & 0.000299 & 512x512x128x139        & 1.870e8 \\
       Core i5 w/ AVX2 Single-Threaded   & 7.817    & 0.000301 & 512x512x128x139        & 5.965e8 \\
       Core i5 w/ AVX2 Multi-Threaded    & 1.804    & 0.000301 & 512x512x128x139        & 2.584e9 \\
       Xeon Phi                          & 0.132    &        - & 256x256x94x72          & 3.360e9 \\
       Xeon Multi-Threaded               & 0.866    &        - & 256x256x94x72          & 5.121e8 \\
       Xeon Single-Threaded              & 3.548    &        - & 256x256x94x72          & 1.250e8 \\
       (Xeon) ITK Single-Threaded        & 14.35    &        - & 512x512x128x42         & 9.820e7 \\
       (Xeon) Native Single-Threaded     & 7.66     &        - & 512x512x128x42         & 1.839e8 \\
       CUDA (Tesla C2050 \cite{gpu_tps}) & 0.252    &        - & 256x256x94x75          & 1.833e9
   \end{tabular}
   \caption{Showing the runtime for volumes of different volume sizes and architectures and the corresponding VCPT.}
   \label{table:archit_runtime}
\end{figure}

\begin{figure} 
    \centering
    \begin{tabular}{cc}
        \includegraphics[width=0.5\textwidth]{6/fig/lung_3d_1.png} & \includegraphics[width=0.5\textwidth]{6/fig/lung_3d_2} \\
        (a) & (b) \\
        \includegraphics[width=0.5\textwidth]{6/fig/lung_3d_3.png} & \includegraphics[width=0.5\textwidth]{6/fig/lung_3d_4} \\
        (c) & (d) 
    \end{tabular}
    \caption{Different views of the right lung from the body cavity.  Control points are marked in yellow as the current points, and red as the target points.  The majority of the movement comes from the diaphram.}
    \label{fig:3d_lung_views}
\end{figure}

\begin{figure}
    \centering
    \begin{tabular}{ccc}
        \includegraphics[width=0.33\textwidth]{6/fig/original.png} & 
        \includegraphics[width=0.33\textwidth]{6/fig/warped.png} &
        \includegraphics[width=0.33\textwidth]{6/fig/target.png} \\
        (a) Original & (b) Warped & (c) Target
    \end{tabular}
    \caption{Display of original, warped and target lung slices. Note that in the warped slice (b), the diaphragm has been adjusted to match the target (c).}
    \label{fig:owt_display}
\end{figure}

\begin{figure}
    \centering
    \begin{tabular}{cc}
        \includegraphics[width=0.5\textwidth]{6/fig/diff_pre_xform.png} & \includegraphics[width=0.5\textwidth]{6/fig/diff_post_xform.png} \\
        (a) Un-warped & (b) Warped 
    \end{tabular}
    \caption{Difference in an image slice of the body cavity before and after the thin-plate spline warp.}
    \label{fig:diff_pre_post_xform}
\end{figure}

An important note about the accuracy of warps is that while the thin-plate spline is able to easily resolve large global errors as seen between figures \ref{fig:owt_display} (a) and (b), it naturally induces smaller local errors all over the volume.  Unfortunately this is the nature of the thin-plate spline.  The control point pinning scheme that is present in this implementation does help to prevent part of this movement by pinning areas of the two un-warped volumes together prior to the warp.  Unfortunately this pinning scheme restricts some of the global movement by potentially prematurely pinning points.  

Given the localized noise created by the deformation across the whole volume, the thin-plate spline would have a much better output if it was instead run as a primary registration technique to obtain a fast global correction to many features.  Following this, a bspline algorithm could be run in order to correct the induced local errors.  Figure \ref{fig:diff_pre_post_xform} shows the diff between the target image and the transformed image, as well as the difference between the target image and the untransformed image.  This clearly demonstrates the way in which the thin-plate spline is able to correct large errors with in the volume with a continuous gradient, however the area that is corrected is left with noise.  This is the noise that could be fixed easily with an iterative bspline, but would require hundreds more thin plate spline control points. 

\begin{figure}
    \centering
    \begin{tabulary}{\linewidth}{L||R|R|R|R|R}
        Vectorization & Width & ST  & MT & OpenMP Speedup & Speedup over Scalar \\
        \hline
        None          & 1     & 60.474  & 15.577            & 3.882          & 1.000 \\
        SSE4.2        & 4     & 20.788  & 5.180             & 4.013          & 3.007 \\
        AVX           & 8     & 9.609   & 2.602             & 3.692          & 5.986 \\
        AVX2          & 8     & 7.180   & 1.804             & 4.333          & 8.634
    \end{tabulary}
    \caption{Effect of vectorization and OpenMP on thin-plate spline warp.}
    \label{table:tps_vec_time}
\end{figure}

On the CPU, the effect of vectorization on the thin-plate spline volume warp is much more pronounced.  Using a 512x512x128 volume with 138 control points, the speedup of vectorization is determined by testing the thin-plate spline warp with no vectorization, SSE4.2, AVX, and AVX2, as seen in table \ref{table:tps_vec_time}.  The increased level of vectorization shows a linear trend against run time, which suggests that the efficiency of vectorization on the Xeon Phi will be similar.  One way in which this will change on the Core i5 against the Xeon Phi, is that the Xeon Phi handles instructions in-order, uses a 512-bit vector unit, and has four threads per core.  Regardless, this shows that the thin-plate spline algorithm, at least on the CPU, scales ideally with the size of the vector unit.  

\begin{figure}
    \centering
    \begin{tabular}{l||r|rlrrr||r|r}
        & \multicolumn{2}{c}{Time} &&&& & \multicolumn{2}{c}{Error} \\
        \cline{0-2} \cline{7-9}
        & ST & MT &&&& & ST & MT \\
        Norm & 70.859 & 19.013 &&&& Norm & 0.000291 & 0.000291 \\
        Fast & 33.761 &  8.459 &&&& Fast & 0.000294 & 0.000294
    \end{tabular}
    \caption{Comparison between time and error when switching between math.h's \codebox{logf} function, and the \codebox{fastlog} function.}
    \label{table:fast_log_err}
\end{figure}

Finally, the \codebox{fastlog} function was able to provide significant performance improvements, with little accuracy loss.  Table \ref{table:fast_log_err} shows that by switching to using  the \codebox{fastlog} function, the performance of the thin-plate spline doubled, both for the single-threaded and multi-threaded case.  Additionally, error values were not significantly impacted with this change.  Visibly examining the volumes, there is no discernible difference to the human eye.  The fastmath package \cite{fastmath} does have SSE extensions available, however there are no extensions available for AVX or AVX2, and so these were tested without vectorization of the \codebox{fastlog} function.

%: ---------------------------------------------------------------------------
%: ----------------------- end of thesis sub-document ------------------------
%  ---------------------------------------------------------------------------
