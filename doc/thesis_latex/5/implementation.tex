% this file is called up by thesis.tex
% content in this file will be fed into the main document

%: ----------------------- name of chapter  -------------------------
\chapter{Implementation}

% ---------------------------------------------------------------------------
% ----------------------- start of chapter content --------------------------
% ---------------------------------------------------------------------------

\section{Overview}

The program with which the thin-plate spline was tested was implemented in the Vala \cite{vala_page} and C programming languages.  As performance extensions, it also uses Intel's Cilk Plus \cite{cilk_plus}, OpenMP 4, and AVX2/512 vectorization.  The compiler used for all testing of the ITK and native implementations was Intel's C Compiler (icc), which shows significant performance gains over the GNU Compiler Collection (gcc), which are gained through auto-vectorization and intel-specific optimization.  All performance critical code was written in C for optimal runtime.  Performance was profiled using a timing functions wrapped around relevant sections.  The program was configured to read and write volumetric images in the MHA format.  For both the CPU and MIC implementations, the code is parallelized with OpenMP and vector instructions, however Xeon Phi has many more cores, and scales much better to applications where there may be hundreds of threads present, as each core is able to handle four threads at a time.  As there can be a difference in performance, it should be noted that the interpolation method used at the destination points of the resultant volume is the trilinear interpolation.
 
\section{CPU Variation}

The CPU implementation of the thin-plate spline algorithm has no significant changes over the standard implementation for the CPU.  The implementation is native, as to get the best performance out of the CPU.  The CPU implementation made use of all available options, aside for the Xeon Phi and fast math instructions.  Two models of CPUs were used.  The CPU present on the system with the Xeon Phi is the Intel Xeon E5-2609 at 2.50GHz.  The other CPU is the Intel Core i5-4670K, which was chosen for compatibility with AVX2 instructions.  The main advantage of the CPU over the Phi is that it has a lower cold startup time, as memory can be directly accessed from main memory, where as the Phi will need to load that memory over the PCIe system bus before acting on it.  Given that the operation isn't especially memory intensive, with a 512x512x128 volume in memory consuming only 128MiB, the bottleneck in the volume warp is necessarily the calculation of the per-voxel warp distance.

\begin{figure}
    \centering
    \lstset{escapechar=@,style=customc,lineskip=-1.5pt}
    \renewcommand\mylstcaption{OpenMP Parallelization}
    \begin{lstlisting}[language=C, caption=\mylstcaption, label=lst:omp_tps]
void warp_volume(float *input, float *output, float *weight_x, float *weight_y, float *weight_z, long int control_points, long int *dim, float *coeffs, float span, float *spacing, float *origin)
{
    .. .
    ...
    #pragma omp parallel for \
        collapse(3)          \
        default(none)        \
        private(...)         \
        shared(...)
    for(int x = 0; x < dim[0]; x++)
    {
        ...
        for(int y = 0; y < dim[1]; y++)
        {
            ...
            for(int z = 0; z < dim[2]; z++)
            {
                //initial points
                ...
                for(int i = 0; i < control_points; i++)
                {
                    ...
                }

                trilinear_interpolate(input, output, dim, sxyz);
            }
        }
    }
}
    \end{lstlisting}
\end{figure}

\begin{figure}
    \centering
    \lstset{escapechar=@,style=customc,lineskip=-1.5pt}
    \renewcommand\mylstcaption{Vectorization of Control Point loop}
    \begin{lstlisting}[language=C, caption=\mylstcaption, label=lst:vec_tps]
void warp_volume(float *input, cloat *output, float *weight_x, float *weight_y, float *weight_z, long int control_points, long int *dim, float *coeffs, float span, float *spacing, float *origin)
{
    ...
    float s0 = 0;
    float s1 = 0;
    float s2 = 0;
    ...
    for(int x = 0; x < dim[0]; x++)
    {
        ...
        for(int y = 0; y < dim[1]; y++)
        {
            ...
            for(int z = 0; z < dim[2]; z++)
            {
                ...
                #pragma omp simd reduction(+:s0,s1,s2)
                for(int i = 0; i < n; i++)
                {
                    scale = ...
                    s0 += scale * weight_x[i];
                    s1 += scale * weight_y[i];
                    s2 += scale * weight_z[i];
                }

                trilinear_interpolate(input, output, dim, sxyz);
            }
        }
    }
}
    \end{lstlisting}
\end{figure}

\section{MIC Variation}

Given the unique architecture of the Xeon Phi architecture, it is necessary to use a many-thread, highly vectorized implementation.  Toward this end, code compiled for the Xeon Phi must take advantage of this vectorization in order to surpass the performance of the CPU.  The Xeon Phi model that is running in the test system is the Xeon Phi 3120A, which has 57 cores, each with a 512-bit vector unit.  Each core on the Xeon Phi may run at 1.1GHz with up to four in-order threads.  When using the Xeon Phi, there are many similarities to using CUDA, in that data must be offloaded to the device and then calls are made to the device.  One key difference between the CUDA architecture and the Xeon Phi is that all Xeon Phi code may be written with inline C code and offload calls are handled with \codebox{\#pragma offload \ldots} calls.  

\begin{figure}
    \centering
    \lstset{escapechar=@,style=customc,lineskip=-1.5pt}
    \renewcommand\mylstcaption{Xeon Phi Offloading}
    \begin{lstlisting}[language=C, caption=\mylstcaption, label=lst:phi_tps]
void warp_volume(float *input, cloat *output, float *weight_x, float *weight_y, float *weight_z, long int control_points, long int *dim, float *coeffs, float span, float *spacing, float *origin)
{
    ...
    #pragma offload target(mic)     \
        in(input : length(0) REUSE) \
        in(weight_x : length(n+4)   \
        in(weight_y : length(n+4)   \
        in(weight_z : length(n+4)   \
        in(coeffs : length(n*6))    \
        out(output : length(lim))
    {
        for(int x = 0; x < dim[0]; x++)
        {
            ...
            for(int y = 0; y < dim[1]; y++)
            {
                ...
                for(int z = 0; z < dim[2]; z++)
                {
                    ...
                    for(int i = 0; i < n; i++)
                    {
                        scale = ...
                        s0 += scale * weight_x[i];
                        s1 += scale * weight_y[i];
                        s2 += scale * weight_z[i];
                    }

                    trilinear_interpolate(input, output, dim, sxyz);
                }
            }
        }
    }
}
    \end{lstlisting}
\end{figure}

\section{Fast Math}

After profiling the running application on the CPU, the piece of the application that consumed the greatest portion of time was the log function from the math library.  Knowing this, the log function provided with the math library was replaced with the \codebox{fastlog} function \cite{fastmath}.  This function introduces little additional error, but cut runtime by two thirds.  This method, as authored by Paul Minerio, worked for simpler SIMD instructions sets, but broke with more advanced sets like AVX.  It is possible that these fastmath extensions could be reworked in order to obtain the same speedup with vector processors.

\section{Vectorization}

Vectorization can be gained on a variety of systems through a few sets of specialized instruction sets.  When working on the CPU, the degree of vectorization is more limited, however with newer processor implementations, this could change.  The processor used in the test system was capable of SSE, SSE2, SSE3, SSE4.1, SSE4.2, and AVX.  The Xeon Phi's vector processing units are capable of 512-bit vector processing, thereby handling up to 16 single-precision calculations at a time.  The vectorization is most useful for assessing the warp distance for each control point, as this operation is independent for each control point and each voxel, and requires no conditional blocks outside of the loop control.

\section{Cilk}

While Cilk Plus is not yet well supported across compilers, using a recent compiler (icc 15.0.3), the Cilk Plus platform allows for a vectorization-independent description of code flow.  Based on the hardware capability that the CPU has, the Cilk Plus platform is able to provide customized vectorization.  As such, a single AVX512 instruction could be decomposed into two AVX/2 instructions, or four SSE instructions.  This helps to promote code reuse, and allows the compiler to handle minor optimization details.

% ---------------------------------------------------------------------------
% ----------------------- end of thesis sub-document ------------------------
% ---------------------------------------------------------------------------
