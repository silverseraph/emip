%---------------------------------------------------------------
% modifed to Drexel standards by James Shackleford 2011
% PhDthesis version 2 style file by Jakob Suckale, 2007
% based on CUEDthesis version 1 by Harish Bhanderi 2002
%---------------------------------------------------------------



%-------------------------- identification ---------------------
%\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{Latex/Classes/PhDthesisPSnPDF}[2011/10/18 v3 PhD thesis class]


%:-------------------------- report or book -----------------------

%If you want to use a Report style document then uncomment the following 3 lines and comment the below 8 book style lines:
%\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
%\ProcessOptions\relax
%%\LoadClass[a4paper]{report}
%\ifx\pdfoutput\undefined
%   \LoadClass[dvips, a4paper]{report}
%\else
%   \LoadClass[pdftex, a4paper]{report}
%\fi

%%Bibliography
%%uncomment next line to change bibliography name to references for Report document class
%\renewcommand{\refname}{References}

%If you want to use a Book style document then uncomment the following 3 lines and comment the above 8 report style lines:
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions\relax
\ifx\pdfoutput\undefined
   \LoadClass[dvips, letterpaper]{book}
\else
   \LoadClass[pdftex, letterpaper]{book}
\fi


%:-------------------------- packages for fancy things -----------------------


\usepackage{amssymb}
\usepackage{amsmath}
%\usepackage{amsthm}
\usepackage{graphics} % for improved inclusion of graphics
%\usepackage{wrapfig} % to include figure with text wrapping around it
\usepackage{subfigure}
\usepackage[margin=10pt,font=small,labelfont=bf]{caption} % for improved layout of figure captions with extra margin, smaller font than text
\usepackage{fancyhdr} % for better header layout
\usepackage{eucal}
\usepackage[english]{babel}
\usepackage[usenames, dvipsnames]{color}
\usepackage[perpage]{footmisc}
\usepackage[square, sort, numbers]{natbib}
\usepackage{ifthen}
\usepackage{multicol} % for pages with multiple text columns, e.g. References
\setlength{\columnsep}{20pt} % space between columns; default 10pt quite narrow
\usepackage[nottoc]{tocbibind} % correct page numbers for bib in TOC, nottoc suppresses an entry for TOC itself
%\usepackage{nextpage}
%\usepackage{sectsty}
%\usepackage{titlesec}
\usepackage{algorithm}
\usepackage{algorithmic}

%:-------------------------- Glossary/Abbrev./Symbols -----------------------

\usepackage[intoc]{nomencl} % load nomencl extension; include in TOC
%\nomrefpage % to include page numbers after abbrevations
\renewcommand{\nomname}{Glossary} % rename nomenclature
\renewcommand{\nomlabel}[1]{\textbf{#1}} % make abbreviations bold
\makenomenclature % used to be \makeglossary
\newcommand{\g}{\footnote{For all abbreviations see the glossary on page \pageref{nom}.}} % type "\g" to refer to glossary

% used to be for sorting into categories:
%\renewcommand\nomgroup[1]{%
%  \ifthenelse{\equal{#1}{A}}{%
%   \item[\textbf{Roman Symbols}] }{%             A - Roman
%    \ifthenelse{\equal{#1}{G}}{%
%     \item[\textbf{Greek Symbols}]}{%             G - Greek
%      \ifthenelse{\equal{#1}{R}}{%
%        \item[\textbf{Superscripts}]}{%              R - Superscripts
%          \ifthenelse{\equal{#1}{S}}{%
%           \item[\textbf{Subscripts}]}{{%             S - Subscripts
%	    \ifthenelse{\equal{#1}{X}}{%
%	     \item[\textbf{Other Symbols}]}{{%    X - Other Symbols
%	    \ifthenelse{\equal{#1}{Z}}{%
%	     \item[\textbf{Acronyms}]}%              Z - Acronyms
%              			{{}}}}}}}}}}


%:-------------------------- PDF/PS setup -----------------------

%if you use a macTeX 2008 or later, use the ifpdf package
\usepackage{ifpdf} 

%if you use an older version, uncomment these lines:
%\newif \ifpdf
%    \ifx \pdfoutput \undefined
        % for running latex
%        \pdffalse
%    \else
        % for running pdflatex
%        \pdfoutput = 1    % positive value for a PDF output
                          % otherwise a DVI
%        \pdftrue
%\fi

\ifpdf
%-->
%--> Google.com search "hyperref options"
%--> 
%--> http://www.ai.mit.edu/lab/sysadmin/latex/documentation/latex/hyperref/manual.pdf
%--> http://www.chemie.unibas.ch/~vogtp/LaTeX2PDFLaTeX.pdf 
%--> http://www.uni-giessen.de/partosch/eurotex99/ oberdiek/print/sli4a4col.pdf
%--> http://me.in-berlin.de/~miwie/tex-refs/html/latex-packages.html
%-->
    \usepackage[ pdftex, plainpages = false, pdfpagelabels, 
                 pdfpagelayout = SinglePage,
                 bookmarks,
                 bookmarksopen = true,
                 bookmarksnumbered = true,
                 breaklinks = true,
                 linktocpage,
                 pagebackref,
                 colorlinks = false,  % was true
                 linkcolor = blue,
                 urlcolor  = blue,
                 citecolor = red,
                 anchorcolor = green,
                 hyperindex = true,
                 hyperfigures
                 ]{hyperref} 

    \DeclareGraphicsExtensions{.png, .jpg, .jpeg, .pdf, .gif} %GIF doesnt work??
    \usepackage[pdftex]{graphicx}
    \pdfcompresslevel=9
    \graphicspath{{0_frontmatter/figures/PNG/}{0_frontmatter/figures/PDF/}{0_frontmatter/figures/}}
\else
    \usepackage[ dvips, 
                 bookmarks,
                 bookmarksopen = true,
                 bookmarksnumbered = true,
                 breaklinks = true,
                 linktocpage,
                 pagebackref,
                 colorlinks = true,
                 linkcolor = blue,
                 urlcolor  = blue,
                 citecolor = red,
                 anchorcolor = green,
                 hyperindex = true,
                 hyperfigures
                 ]{hyperref}

    \DeclareGraphicsExtensions{.eps, .ps}
    \usepackage{epsfig}
    \usepackage{graphicx}
    \graphicspath{{0_frontmatter/figures/EPS/}{0_frontmatter/figures/}}
\fi


%:-------------------------- page layout -----------------------

%: JAS 2011.10.18
%:   Added 8.5 x 11 settings

%Letter (8.5 x 11) settings
\ifpdf
   \pdfpageheight=11in
   \pdfpagewidth=8.5in
\else
   \setlength{\paperheight}{11in}
   \setlength{\paperwidth}{8.5in}
\fi


%A4 settings
%\ifpdf
%   \pdfpageheight=297mm
%   \pdfpagewidth=210mm
%\else
%   \setlength{\paperheight}{297mm}
%   \setlength{\paperwidth}{210mm}
%\fi

\setlength{\hoffset}{0.00cm}
\setlength{\voffset}{0.00cm}

%: Uncomment this secion for two-sided printing
% ------------------------------
%\setlength{\oddsidemargin}{1.5cm}
%\setlength{\evensidemargin}{0cm}
%\setlength{\topmargin}{1mm}
%\setlength{\headheight}{1.36cm}
%\setlength{\headsep}{1.00cm}
%\setlength{\textheight}{20.84cm}
%\setlength{\textwidth}{14.5cm}
%\setlength{\marginparsep}{1mm}
%\setlength{\marginparwidth}{3cm}
%\setlength{\footskip}{2.36cm}


%: JAS 2011.10.18
%:   Rewrote these margins for US Letter paper (8.5 x 11)

%: Uncomment this secion for one-sided printing
% taken from the original file, but with the first two lanes modified
% ------------------------------
\setlength{\evensidemargin}{0.5in}   % latex adds 1 in to this!
\setlength{\oddsidemargin}{0.5in}    % latex adds 1 in to this!
\setlength{\topmargin}{-0.10in}      % latex adds 1 in to this!
\setlength{\headheight}{0.25in}
\setlength{\headsep}{0.25in}
\setlength{\textheight}{8.70in}
\setlength{\textwidth}{6.0in}
\setlength{\marginparsep}{0in}
\setlength{\marginparwidth}{0in}
\setlength{\footskip}{1in}


%: JAS 2011.10.18
%:   Modified header & footer to comply with drexel pagination style

%: section below defines fancy page layout options
% ------------------------------
\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{\markboth{\MakeUppercase{\thechapter. #1 }}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}
\fancyhf{}
\fancyhead[L]{\bfseries\leftmark}
\fancyhead[R]{\thepage}
%\fancyhead[R]{\bfseries\rightmark}
%\fancyfoot[C]{\thepage}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0pt}
\addtolength{\headheight}{0.5pt}
\fancypagestyle{plain}{
  \fancyhead{}
  \renewcommand{\headrulewidth}{0pt}
}





%:-------------------------- title page layout -----------------------

% starts roman page numbering until chapter 1
% important to avoid two pages numbered 1 and 2 which may cause bad links
% bug: cover i + back side ii and then numbering restarts with i; should be iii
\renewcommand{\thepage}{\roman{page}}

\newcommand{\submittedtext}{{A thesis submitted for the degree of}}

% DECLARATIONS
% These macros are used to declare arguments needed for the
% construction of the title page and other preamble.

% The year and term the degree will be officially conferred
\def\degreedate#1{\gdef\@degreedate{#1}}
% The full (unabbreviated) name of the degree
\def\degree#1{\gdef\@degree{#1}}
% The name of your college or department(eg. Trinity, Pembroke, Maths, Physics)
\def\collegeordept#1{\gdef\@collegeordept{#1}}
% The name of your University
\def\university#1{\gdef\@university{#1}}
% Defining the crest
\def\crest#1{\gdef\@crest{#1}}
% Stating the city of birth for title page where needed; uncommented for use
%\def\cityofbirth#1{\gdef\@cityofbirth{#1}}

\def\AuthorEmail#1{\gdef\@AuthorEmail{#1}}
\def\AuthorName#1{\gdef\@AuthorName{#1}}
\def\UnivName#1{\gdef\@UnivName{#1}}
\def\UnivURL#1{\gdef\@UnivURL{#1}}
\def\DeptName#1{\gdef\@DeptName{#1}}
\def\DeptURL#1{\gdef\@DeptURL{#1}}

% These macros define an environment for front matter that is always 
% single column even in a double-column document.

\newenvironment{alwayssingle}{%
       \@restonecolfalse\if@twocolumn\@restonecoltrue\onecolumn
       \else\newpage\fi}
       {\if@restonecol\twocolumn\else\newpage\fi}

%define title page layout
\renewcommand{\maketitle}{%
\begin{alwayssingle}
    \renewcommand{\footnotesize}{\small}
    \renewcommand{\footnoterule}{\relax}
    \thispagestyle{empty}
%  \null\vfill
  \begin{center}
  {\large
    {{\bfseries {\@title}} \par}
	\vspace*{3ex}
    {{A Thesis} \par}
	\vspace*{2ex}
    {{Submitted to the Faculty} \par}
	\vspace*{2ex}
    {{of} \par}
	\vspace*{2ex}
    {{\@university} \par}
	\vspace*{2ex}
    {{by} \par}
	\vspace*{2ex}
    {{\@author} \par}
	\vspace*{2ex}
    {{in partial fulfillment of the} \par}
	\vspace*{2ex}
    {{requirements for the degree} \par}
	\vspace*{2ex}
    {{of} \par}
	\vspace*{2ex}
    {{\@degree} \par}
	\vspace*{2ex}
    {\@degreedate}
  }
  \end{center}
  \null\vfill
\end{alwayssingle}

% Copyright Page
\newpage
\thispagestyle{empty}
\begin{center}
    \vspace*{2.5in}
    \copyright \hspace{1pt} Copyright 2015 \par
    R. Andrew Benton II. All Rights Reserved.
\end{center}
}

% LOGO VERSION
%\renewcommand{\maketitle}{%
%\begin{alwayssingle}
%    \renewcommand{\footnotesize}{\small}
%    \renewcommand{\footnoterule}{\relax}
%    \thispagestyle{empty}
%%  \null\vfill
%  \begin{center}
%    { \Large {\bfseries {\@title}} \par}
%{\large \vspace*{20mm} {{\@crest} \par} \vspace*{20mm}}
%    {{\Large \@author} \par}
%%	\vspace*{1ex}
%%	{{\@cityofbirth} \par}    
%{\large 
%	\vspace*{1ex}
%    {{\@collegeordept} \par}
%	\vspace*{1ex}
%    {{\@university} \par}
%	\vspace*{25mm}
%    {{\submittedtext} \par}
%	\vspace*{1ex}
%    {\it {\@degree} \par}
%	\vspace*{2ex}
%    {\@degreedate}
%}%end large
%  \end{center}
%  \null\vfill
%\end{alwayssingle}}

% page number for cover back side should have page number blanked


%:-------------------------- front matter layout -----------------------

% DEDICATION
%
% The dedication environment makes sure the dedication gets its
% own page and is set out in verse format.

\newenvironment{dedication}
{
    \begin{alwayssingle}
%    \pagestyle{empty}
    \begin{center}
        \vspace*{1.5cm}
        {\large \bfseries DEDICATION}
    \end{center}
    \vspace{0.5cm}
    \begin{quote}
    \begin{center}
} {
    \end{center}
    \end{quote}
    \end{alwayssingle}
}


% ACKNOWLEDGEMENTS
%
% The acknowledgements environment puts a large, bold, centered 
% "Acknowledgements" label at the top of the page. The acknowledgements
% themselves appear in a quote environment, i.e. tabbed in at both sides, and
% on its own page.

\newenvironment{acknowledgements}
{
%    \pagestyle{empty}
    \begin{alwayssingle}
    \begin{center}
        \vspace*{1.5cm}
        {\large \bfseries ACKNOWLEDGEMENTS}
    \end{center}
    \vspace{0.5cm}
    \begin{quote}
} {
    \end{quote}
    \end{alwayssingle}
}

% The acknowledgementslong environment puts a large, bold, centered 
% "Acknowledgements" label at the top of the page. The acknowledgement itself 
% does not appears in a quote environment so you can get more in.

\newenvironment{acknowledgementslong}
{\pagestyle{empty}
\begin{alwayssingle}
\begin{center}
\vspace*{1.5cm}
{\Large \bfseries Acknowledgements}
\end{center}
\vspace{0.5cm}\begin{quote}}
{\end{quote}\end{alwayssingle}}

%ABSTRACT
%
%The abstract environment puts a large, bold, centered "Abstract" label at
%the top of the page. The abstract itself appears in a quote environment,
%i.e. tabbed in at both sides, and on its own page.

\newenvironment{abstracts}
{
    \begin{alwayssingle}
    \thispagestyle{empty}
    \begin{center}
        \vspace*{1.5cm}
        \renewcommand\baselinestretch{1.00}
        \baselineskip=14pt plus1pt
        {{\large \bfseries  ABSTRACT} \par}
        {\large
          {{\@title} \par}
          {{R. Andrew Benton II} \par}
          {{James Anthony Shackleford, Ph.D}}
        }
    \end{center}

    \markboth{\MakeUppercase{Abstract}}{}
    \addcontentsline{toc}{chapter}{Abstract}

    \renewcommand\baselinestretch{1.62}
    \baselineskip=28pt plus1pt
    \vspace{0.5cm}
%    \begin{quote}
} {
%    \end{quote}
    \end{alwayssingle}
}

%The abstractlong environment puts a large, bold, centered "Abstract" label at
%the top of the page. The abstract itself does not appears in a quote
%environment so you can get more in.

\newenvironment{abstractslong} {\begin{alwayssingle} \pagestyle{empty}
  \begin{center}
  \vspace*{1.5cm}
  {\Large \bfseries  Abstract}
  \end{center}
  \vspace{0.5cm} \begin{quote}}
{\end{quote}\end{alwayssingle}}

%The abstractseparate environment is for running of a page with the abstract
%on including title and author etc as required to be handed in separately

\newenvironment{abstractseparate} {\begin{alwayssingle} \pagestyle{empty}
  \vspace*{-1in}
 \begin{center}
    { \Large {\bfseries {\@title}} \par}
    {{\large \vspace*{1ex} \@author} \par}
{\large \vspace*{1ex}
    {{\@collegeordept} \par}
    {{\@university} \par}
\vspace*{1ex}
    {{\it \submittedtext} \par}
    {\it {\@degree} \par}
\vspace*{2ex}
    {\@degreedate}}
  \end{center}}
{\end{alwayssingle}}

%Statement of originality if required

\newenvironment{declaration} {\begin{alwayssingle} \pagestyle{empty}
  \begin{center}
  \vspace*{1.5cm}
  {\Large \bfseries  Declaration}
  \end{center}
  \vspace{0.5cm}
   \begin{quote}}
{\end{quote}\end{alwayssingle}}


% PHD VITA ENVIRONMENT
\newenvironment{vita}
{
    \begin{alwayssingle}
    \thispagestyle{empty}
    \begin{center}
        \vspace*{1.5cm}
        \renewcommand\baselinestretch{1.00}
        \baselineskip=14pt plus1pt
        {{\large \bfseries  VITA} \par}
    \end{center}

    \markboth{\MakeUppercase{Vita}}{}
    \addcontentsline{toc}{chapter}{Vita}

    \renewcommand\baselinestretch{1.62}
    \baselineskip=28pt plus1pt
    \vspace{0.5cm}
} {
    \end{alwayssingle}
}


%: JAS 2011.10.18
%:---------- redefine headers to meet drexel requirements ---------------------
%:             ( fiercely ugly though they may be... )

%: Note, these all fall out of library standards compliance if a document
%: font size greater than 11pt is imposed.

% for Contents, List of Figures, List of Tables, etc
\renewcommand{\@makeschapterhead}[1]{%
    \vspace*{50\p@}%
    {\parindent \z@ \centering
        \large
        \interlinepenalty\@M
        \bfseries \MakeUppercase{#1}
        \vskip 40\p@
    }
}

% for \chapters
\renewcommand{\@makechapterhead}[1]{%
    \vspace*{50\p@}%
    {\parindent \z@ \centering
        \large
        \interlinepenalty\@M
        \bfseries \MakeUppercase{\@chapapp\space \thechapter : #1}
        \vskip 40\p@
    }
}

% for \sections
\def\section{\@startsection {section}
    {1}                                 % level
    {\z@}                               % indent (0pt)
    {-3.5ex plus -1ex minus -.2ex}      % beforeskip
    {2.3ex plus .2ex}                   % afterskip
    {\bf\large}                         % style
}
\def\subsection{\@startsection{subsection}
    {2}                                 % level
    {\z@}                               % indent (0pt)
    {-3.25ex plus -1ex minus -.2ex}     % beforeskip
    {1.5ex plus .2ex}                   % afterskip
    {\bf\large}                         % style
}
\def\subsubsection{\@startsection{subsubsection}
    {3}                                 % level
    {\z@}                               % indent (0pt)
    {-3.25ex plus -1ex minus -.2ex}     % beforeskip
    {1.5ex plus .2ex}                   % afterskip
    {\bf\large}                         % style
}


%:-------------------------- page numbers: roman+arabic -----------------------

%: JAS 2011.10.24
%:   redefine \frontmatter so that roman numerials begin at 2
%    it is required by drexel that the title page count as
%    "Page i" but not be labeled as such.

\renewcommand\frontmatter{\pagenumbering{roman}\setcounter{page}{1}}
\newpage\renewcommand{\thepage}{\arabic{page}}\setcounter{page}{1}
