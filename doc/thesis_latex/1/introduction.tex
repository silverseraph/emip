% this file is called up by thesis.tex
% content in this file will be fed into the main document

%: ----------------------- introduction file header -----------------------
\chapter{Introduction}

% the code below specifies where the figures are stored
\ifpdf
    \graphicspath{{1_introduction/figures/PNG/}{1_introduction/figures/PDF/}{1_introduction/figures/}}
\else
    \graphicspath{{1_introduction/figures/EPS/}{1_introduction/figures/}}
\fi

% ----------------------------------------------------------------------
%: ----------------------- introduction content -----------------------
% ----------------------------------------------------------------------

Image registration, in general, focuses on aligning multiple images to one another based on a vector field that defines a spatial deformation.  In medical imaging, this can provide insight into the patient's state for physicians and technicians, in addition to providing valuable medical information without the need of as much interference from human agents.  

Image registration can aid before and during the treatment process.  During the treatment process, it can be used to map between previously obtained images and images that are currently being obtained. Prior to treatment, this can also be used to map landmarks and annotations from one image volume to another.  This annotation is usually performed by a trained technician in order to highlight points of interest including major organs and tumors.  Automating this process can aid in increasing clinical throughput, as each volume will usually take a human two to three hours to complete.

Ideally, image registration would achieve a perfect match between the two images modalities, providing a vector deformation map without errors.  In reality, this will never happen, as the material being imaged will experience a change in content between when the images are captured.  Instead, most registration techniques propose to create a solution and then iteratively improve the proposed transform to minimize the degree of error.  Overtime such an iterative process can converge to the global minimum of error by making alterations in the proposed control point parameters such that the regions in the area of each control point reaches a smaller error value.  

A bspline deformation works by imposing a grid of control points over the volume to be deformed, and then adjusting the coefficients at these grid points until the resultant spatial deformation reaches the desirable range.  The size of the grid may change, and increasing the size of the grid causes the spatial transform to be more localized, and allows for more rapid shifts.  While the bspline transform is differentiable it is only twice differentiable.  The value for the spatial transform is determined by passing the coefficients into the bspline function, often cubic, to determine the spatial shift at each voxel.  The disadvantage to using the bspline then is that the locality of the deformation is limited by the degree to which the bspline can handle higher noise deformations.  

An alternative method of spatial deformation is the thin plate spline.  The thin plate spline works by selecting pairs of points on a global level.  These pairs of points are used on a global level to calculate a deformation.  Each point affects every point in the volume, albeit to different degrees based on the distance from the point.  At least $d + 1$ points are required when working with the thin plate spline, where $d$ is the number of dimensions in the data.  The benefit of using the thin plate spline is that the resultant deformations are smooth and infinitely differentiable.  The downside to the thin plate spline is that the runtime of the algorithm on the volume is linear with respect to the number of control points used and the determination of the warping weights is calculated with the LU decomposition.  As such, control points must be chosen sparingly.

Error is typically measured using either mean-squared-error (MSE) or mutual-information (MI).  There is a trade off between these two error measuring techniques.  The MSE method is very fast and works very well with parallelization, however it only works well when comparing image volumes that use the same modality, or use very similar modalities.  The MI technique is very well suited to matching image volumes across different modalities, but it is harder to parallelize due to the MI technique requiring that a probability distribution function (PDF) is generated which relates the probabilities of voxel values between the image volumes.  

Past works in the area of medical image registration have focused on the speed and quality of the produced transforms.  These transformations are computationally intensive, and cannot be run in realtime except on the most powerful hardware.  To this end, it is desirable to find solutions that may be able to compute these transformations using accelerator hardware to increase the rate at which the transformation can be obtained.

In Chapter 2, topics relating to this work are discussed, including the bspline, thin-plate splines on the CUDA architecture, and the MIC architecture.  Chapter 3 introduces theory of the  thin-plate spline algorithm.  Chapter 4 discusses the use of landmarks and control points in the thin-plate spline algorithm, including the way that the size of the problem may be constrained by selectively activating and deactivating control points based on their redundancy.  In Chapter 5 the implementation of the thin-plate spline on the CPU and MIC architecture is discussed, including the way in which the thin-plate benefits from the heavy vectorization potential of the Xeon Phi.  Chapter 6 presents and examines the results of the thin-plate spline for accuracy given a set of points, as well as the run time of the warp.  Chapter 7 analyzes the results of Chapter 6 and presents suggestions and conclusions.  Finally, Chapter 8 suggests the direction of future work on the thin-plate spline algorithm, proposing a global registration technique.

% ---------------------------------------------------------------------------
% ----------------------- end of thesis sub-document ------------------------
% ---------------------------------------------------------------------------
