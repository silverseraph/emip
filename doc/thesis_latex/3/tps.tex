% this file is called up by thesis.tex
% content in this file will be fed into the main document

%: ----------------------- name of chapter  -------------------------
\chapter{Thin Plate Spline Algorithm}


% ---------------------------------------------------------------------------
% ----------------------- start of chapter content --------------------------
% ---------------------------------------------------------------------------

\section{Overview}
The TPS algorithm used here is based off of a 2D version of the TPS algorithm.  In 2D versions of the TPS algorithm, three or more points are used to calculate a warping plane that defines a spatial transform on a 2D surface.  Here, this is altered such that a warping solid is created, which requires four or more points.  These points may be designated by a user, or created algorithmically.  After the TPS algorithm runs, a set of weights in the x, y, and z direction are created.  These weights are then used to compute the deformation of the volume. The number of weights is based off of the number of points given to the TPS algorithm.  As such, adding frivolous points will hurt the performance of the algorithm, as the TPS matrix is solved with LU decomposition.  A method to decimate these points is discussed later.  The TPS algorithm itself is unable to do more than create a spatial deformation when given a set of control points.  In order to create the control points, there needs to be an algorithm that attempts to perform a rough mapping between the two volumes. 

There are a variety of simplifications that can be done to make the thin plate spline faster. It is possible to sacrifice quality of the transform in order to gain a substantial increase in the performance of the algorithm by calculating the magnitude of the weights returned by the TPS algorithm, and then removing some of the least significant points.  Other methods exist that revolve around removing redundant control points in order to reduce the runtime of the TPS transform.  If control points are not reduced such that the least significant points are suspended, then the runtime of the transform will massively dominate the runtime of the whole workflow.  The runtime of the TPS transform on a volume is $O(mn)$, where $m$ is the number of control points, and $n$ is the number of voxels in the transformed image volume.  Considering that at least 8 points are needed in order to hold the volume in place, this number will quickly rise, necessitating a simplification algorithm.  

\section{Coefficient Matrix Construction}
The weights for the thin-plate spline algorithm are derived from a matrix solution.  This matrix solution models the distance between each point using a radial basis function.  In equation \ref{eq:tps}, this models the $K$ submatrix.  The submatrix element values at $[i,j]$ can be determined to be the RBF of the distance between control point $i$ and control point $j$.  As such, the diagonal of $K$ will be zeros.  The values in $P$ are the $x$, $y$, $z$, and a zero value for each control point.  Submatrix $O$ is a $4$x$4$ matrix filled with zeros.  $w$ is the resultant weights that will be used for solving the transform.  Vector $v$ is the initial displacement of the vectors from their original position.  Finally, vector $o$ is a $4$x$1$ vector filled with zeros.  Given this linear system of equations, the weights for the thin-plate spline transform can be determined by using LU-decomposition.

\begin{equation} \label{eq:tps}
    \begin{bmatrix}
        K & P^T \\[0.5em]
        P & O
    \end{bmatrix}
    \begin{bmatrix}
        w 
    \end{bmatrix} = \begin{bmatrix}
        v \\[0.5em]
        o
    \end{bmatrix}
\end{equation}

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{3/fig/rbf_2d.png}
    \caption{2D Radial Basis Function (RBF) for comparison.  Coefficients in the matrix and offset weights in the thin-plate spline warp are calculated using the 3D radial basis function.}
    \label{fig:rbf_2d}
\end{figure}

\section{Thin-Plate Spline Transform}
Given the weights that are extracted from \ref{eq:tps}, the resultant volume can be determined by performing a voxel-wise operation.  For each voxel, the offset from the current voxel is determined as the static offset and linear offset from the original point.  The offset if further modified by scaling each weight by the value of the distance between the current voxel and the landmark analogous to that TPS weight.  Using this offset, which is a floating point value in the x, y, and z direction, and adding it to the current position, the new voxel value at that position is calculated as the interpolation of the target point.  This interpolation may either be a rounding, or any other type of interpolation.  In the implementation here, it is a trilinear interpolation.  A simplified version of the volumetric thin-plate spline warp can be seen in figure \ref{lst:vtps}.

\begin{figure}
    \centering
    \lstset{escapechar=@,style=customc,lineskip=-1.5pt}
    \renewcommand\mylstcaption{Single-threaded volumetric thin-plate spline warp}
    \begin{lstlisting}[language=C, caption=\mylstcaption, label=lst:vtps]
void warp_volume(float *input, float *output, float *weight_x, float *weight_y, float *weight_z, long int control_points, long int *dim, float *coeffs, float span, float *spacing, float *origin)
{
    float pxyz[3] = {0.0f, 0.0f, 0.0f};
    for(int x = 0; x < dim[0]; x++)
    {
        pxyz[0] = (origin[0] + (spacing[0] * x)) / span;
        for(int y = 0; y < dim[1]; y++)
        {
            pxyz[1] = (origin[1] + (spacing[1] * x)) / span;
            for(int z = 0; z < dim[2]; z++)
            {
                pxyz[2] = (origin[2] + (spacing[2] * x)) / span;
                float sum[3] = {weight_x[control_points+0], 
                                weight_y[control_points+0], 
                                weight_z[control_points+0]}
                sum[0] += weight_x[control_points+1] * pxyz[0];
                sum[1] += weight_y[control_points+1] * pxyz[1];
                sum[2] += weight_z[control_points+1] * pxyz[2];
                sum[0] += weight_x[control_points+2] * pxyz[0];
                sum[1] += weight_y[control_points+2] * pxyz[1];
                sum[2] += weight_z[control_points+2] * pxyz[2];
                sum[0] += weight_x[control_points+3] * pxyz[0];
                sum[1] += weight_y[control_points+3] * pxyz[1];
                sum[2] += weight_z[control_points+3] * pxyz[2];

                for(int i = 0; i < control_points; i++)
                {
                    float scale = logf(0.00001 + 
                        (((coeffs[(i*6)+0] - pxyz[0])*(coeffs[(i*6)] + 0] - pxyz[0])) +
                         ((coeffs[(i*6)+1] - pxyz[1])*(coeffs[(i*6)] + 1] - pxyz[1])) +
                         ((coeffs[(i*6)+2] - pxyz[2])*(coeffs[(i*6)] + 2] - pxyz[2]))));
                    sum[0] += weight_x[i] * scale;
                    sum[1] += weight_y[i] * scale;
                    sum[2] += weight_z[i] * scale;
                }

                float sxyz[3] = {
                    (((pxyz[0] + sum[0]) * span) - origin[0]) / spacing[0]
                    (((pxyz[1] + sum[1]) * span) - origin[1]) / spacing[1]
                    (((pxyz[2] + sum[2]) * span) - origin[2]) / spacing[2]
                }

                trilinear_interpolate(input, output, dim, sxyz);
            }
        }
    }
}
    \end{lstlisting}
\end{figure}

% ---------------------------------------------------------------------------
%: ----------------------- end of thesis sub-document ------------------------
% ---------------------------------------------------------------------------
