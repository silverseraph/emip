public class Graph<V,E> 
{
    public Vala.HashMap<V, Vertex<V,E>> vertices;
    public Vala.HashMap<E, Edge<V,E>> edges;

    public Graph(HashFunc vertex_hash_func, EqualFunc vertex_equal_func, HashFunc edge_hash_func, EqualFunc edge_equal_func)
    {
        this.vertices = new Vala.HashMap<V, Vertex<V,E>>(vertex_hash_func, vertex_equal_func);
        this.edges = new Vala.HashMap<E, Edge<V,E>>(edge_hash_func, edge_equal_func);
    }

    public Vertex<V,E> add_vertex(V data)
    {
        var v = new Vertex<V,E>(this, data);

        if(this.vertices.contains(data))
        {
            this.remove_vertex(data);
        }

        this.vertices[data] = v;

        return v;
    }

    public void remove_vertex(V data)
    {
        var vertex = this.vertices[data];

        //remove all edges from this vertex and connected vertices
        foreach(var e in vertex.connected_edges) this.remove_edge(e.data);

        //set this entry in the vertices map to null
        this.vertices.remove(data);
    }

    public Edge<V,E> add_edge(Vertex<V,E> vertex_a, Vertex<V,E> vertex_b, E data)
    {
        Edge<V,E> e = new Edge<V,E>(this, vertex_a, vertex_b, data);

        if(this.edges.contains(data))
        {
            this.remove_edge(data);
        }

        this.edges[data] = e;
        vertex_a.add_edge(e);
        vertex_b.add_edge(e);

        return e;
    }

    public Edge<V,E> add_edge_by_data(V a_data, V b_data, E data)
    {
        var va = vertices[a_data];
        var vb = vertices[b_data];

        assert(va != null);
        assert(vb != null);

        Edge<V,E> e = new Edge<V,E>(this, va, vb, data);

        if(this.edges.contains(data))
        {
            this.remove_edge(data);
        }

        this.edges[data] = e;
        va.add_edge(e);
        vb.add_edge(e);

        return e;
    }

    public void remove_edge(E data)
    {
        var edge = this.edges[data];

        //remove this edge from the adjancency listings for the attached vertices
        var va = edge.vertex_a;
        var vb = edge.vertex_b;

        va.connected_edges.remove_all(edge);
        vb.connected_edges.remove_all(edge);

        //cleanup the edge
        edge.vertex_a = null;
        edge.vertex_b = null;
        edge.graph = null;
        edge.data = null;

        //set this entry in the edges map to null
        this.edges.remove(data);
    }

    public void clear_edges()
    {
        foreach(var ed in this.edges.get_keys())
        {
            this.remove_edge(ed);
        }
    }

    public void clear()
    {
        foreach(var vd in this.vertices.get_keys())
        {
            this.remove_vertex(vd);
        }
    }
}
