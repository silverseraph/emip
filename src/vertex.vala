public class Vertex<V,E>
{
    public Graph<V,E> graph;
    public V data;
    public Vala.ArrayList<Edge<V,E>> connected_edges;

    internal Vertex(Graph<V,E> graph, V data)
    {
        this.graph = graph;
        this.data = data;
        this.connected_edges = new Vala.ArrayList<Edge<V,E>>();
    }

    public void add_edge(Edge<V,E> e)
    {
        this.connected_edges.add(e);
    }
}
