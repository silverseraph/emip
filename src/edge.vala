public class Edge<V,E>
{
    public Graph<V,E> graph;
    public Vertex<V,E> vertex_a;
    public Vertex<V,E> vertex_b;
    public E data;

    internal Edge(Graph<V,E> graph, Vertex<V,E> a, Vertex<V,E> b, E data)
    {
        this.graph = graph;
        this.vertex_a = a;
        this.vertex_b = b;
        this.data = data;
    }
}
