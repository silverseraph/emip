#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <omp.h>
#include <glib.h>

#include "emip_utils.h"

#ifndef __volume_funcs_header__
#define __volume_funcs_header__

void volume_internal_diff(guint64 len, float *da, float *db, float *dr);

void volume_phi_localize(float *data, glong size);

void volume_phi_delocalize(float *data, glong size);

#endif
