using Gsl;

public class TpslmrXform //: Xform
{
    /* CONSTANTS */
    public const long COEFF_SIZE = 6;

    /* FIELDS */
    public long num_coeffs;
    public float* coeffs;
    public string[] coeff_name;
    public RegGroup group;
    public long dim[3];
    public float offset[3];
    public float spacing[3];
    public float span[3];
    public float max_d;
    public float regularize;

    /* PROPERTIES */
    public InterpType interp { get; set; }

    public Tps tps;

    public TpslmrXform()
    {
        this.num_coeffs = 0;
        this.coeffs = (float*)malloc(sizeof(float) * this.num_coeffs * TpslmrXform.COEFF_SIZE);
        this.coeff_name = new string[0];
        for(long d = 0; d < 3; d++)
        {
            this.dim[d] = 1;
            this.offset[d] = 0f;
            this.spacing[d] = 1f;
            this.span[d] = 1f;
        }
        this.max_d = 1;
    }

    public TpslmrXform.as_original(RegGroup group, TpslmrXformMethod method)
    {
        this.group = group;
        this.regularize = method.regularize;
        this.num_coeffs = 0;
        this.coeffs = (float*)malloc(sizeof(float) * this.num_coeffs * TpslmrXform.COEFF_SIZE);
        assert(this.group.target_landmarks != null);
        assert(this.group.original_landmarks != null);

        var tar_lms = this.group.target_landmarks.landmarks;
        var ori_lms = this.group.original_landmarks.landmarks;

        this.dim = new long[]{0, 0, 0};
        for(long d = 0; d < 3; d++) 
        {
            this.dim[d] = this.group.original_volume.dim[d];
            this.offset[d] = this.group.original_volume.offset[d];
            this.spacing[d] = this.group.original_volume.spacing[d];
            this.span[d] = this.spacing[d] * this.dim[d];
        }

        this.max_d = float.max(this.span[0], float.max(this.span[1], this.span[2]));

        this.tps = new Tps(this.max_d, this.regularize);

#if DEBUG_XFORM
        foreach(var pn in tar_lms.get_keys()) stdout.printf("DEBUG: Target landmark name: %s\n", pn);
        foreach(var pn in ori_lms.get_keys()) stdout.printf("DEBUG: Origin landmark name: %s\n", pn);
#endif

        log_nifty("Found %d target landmarks.\n", tar_lms.size);
        log_nifty("Found %d original landmarks.\n", ori_lms.size);

        foreach(var pn in tar_lms.get_keys())
        {
            var point_a = ori_lms[pn];
            if(point_a == null)
            {
                critical("Detected a landmark mismatch from origin landmark, \"%s\". Exiting...\n".printf(point_a.name));
            }
            var point_b = tar_lms[pn];
            if(point_b == null)
            {
                critical("Detected a landmark mismatch from target landmark, \"%s\". Exiting...\n".printf(point_b.name));
            }

            this.add_coeff(
                pn.dup(),
                point_a.x, 
                point_a.y, 
                point_a.z, 
                (point_b.x - point_a.x), 
                (point_b.y - point_a.y),
                (point_b.z - point_a.z));
        }

        this.update_tps();
    }

    public TpslmrXform clone()
    {
#if DEBUG_XFORM
        stdout.printf("TpslmrXform.clone\n");
#endif
        TpslmrXform clone = new TpslmrXform();
        clone.group = this.group;
        clone.num_coeffs = this.num_coeffs;
        clone.coeffs = (float*)malloc(sizeof(float) * clone.num_coeffs * TpslmrXform.COEFF_SIZE);
        clone.coeff_name = new string[clone.num_coeffs];
        clone.max_d = this.max_d;
        clone.regularize = this.regularize;

        for(long d = 0; d < 3; d++) 
        {
            clone.dim[d] = this.dim[d];
            clone.offset[d] = this.offset[d];
            clone.spacing[d] = this.spacing[d];
            clone.span[d] = this.span[d];
        }
        clone.tps = new Tps(clone.max_d, clone.regularize);

        long offset = 0;
        for(long i = 0; i < clone.num_coeffs; i++)
        {
            offset = i * TpslmrXform.COEFF_SIZE;
            for(long j = 0; j < TpslmrXform.COEFF_SIZE; j++)
            {
                clone.coeffs[offset + j] = this.coeffs[offset + j];
            }
            clone.coeff_name[i] = this.coeff_name[i];
        }

        clone.update_tps();

        return clone;
    }

    public string to_string()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("TpsLMRXform:\n");
        sb.append("x,y,z,a,b,c\n");

#if DEBUG_XFORM
        stdout.printf("There are %ld coefficient sets.\n", this.num_coeffs);
#endif

        for(long offset = 0; offset < (this.num_coeffs * TpslmrXform.COEFF_SIZE); offset += 6)
        {
            sb.append("%20s, %8.3g, %8.3g, %8.3g, %8.3g, %8.3g, %8.3g\n".printf(
                    this.coeff_name[offset/TpslmrXform.COEFF_SIZE],
                    this.coeffs[offset + 0],
                    this.coeffs[offset + 1],
                    this.coeffs[offset + 2],
                    this.coeffs[offset + 3],
                    this.coeffs[offset + 4],
                    this.coeffs[offset + 5]));
        }

        return sb.str;
    }

    public void print()
    {
        stdout.puts(this.to_string());
    }

    [CCode(cheader_filename="tps_diff.h", cname="warp_volume")]
    private static extern void warp_volume(float *vol_data_out, float *vol_data_in, double *wx, double *wy, double *wz, float *coeffs, long num_coeffs, float max_d, long *a_dim, float *a_offset, float *a_spacing);

    private void warp_landmarks()
    {
        var vdim = this.group.original_volume.dim;
        var wx = this.tps.wx;
        var wy = this.tps.wy;
        var wz = this.tps.wz;

        float pos[3] = {0, 0, 0};
        float pxyz[3] = {0, 0, 0};
        long dim[3] = {0, 0, 0};
        double sum[3] = {0, 0, 0};
        float *coeffs = this.coeffs;
        float span = this.max_d;
        long offset = 0;
        double dist, scale;

        for(long i = 0; i < 3; i++) dim[i] = vdim[i];

        var ori_lms = this.group.original_landmarks;
        var tmp_lms = new Landmarks();
        tmp_lms.name = ori_lms.name;
        tmp_lms.valid = ori_lms.valid;

        long n = this.num_coeffs;

        foreach(var pn in ori_lms.landmarks.get_keys())
        {
            var point = ori_lms.landmarks[pn].clone();
            assert(point != null);

            pos[0] = point.x;
            pos[1] = point.y;
            pos[2] = point.z;

            pxyz[0] = pos[0] / span;
            pxyz[1] = pos[1] / span;
            pxyz[2] = pos[2] / span;
            
            sum = {0, 0, 0};
            sum[0] += wx[n+0];
            sum[1] += wy[n+0];
            sum[2] += wz[n+0];
            sum[0] += wx[n+1] * ((double)(pxyz[0]));
            sum[1] += wy[n+1] * ((double)(pxyz[0]));
            sum[2] += wz[n+1] * ((double)(pxyz[0]));
            sum[0] += wx[n+2] * ((double)(pxyz[1]));
            sum[1] += wy[n+2] * ((double)(pxyz[1]));
            sum[2] += wz[n+2] * ((double)(pxyz[1]));
            sum[0] += wx[n+3] * ((double)(pxyz[2]));
            sum[1] += wy[n+3] * ((double)(pxyz[2]));
            sum[2] += wz[n+3] * ((double)(pxyz[2]));

#if DEBUG_XFORM
            stdout.printf("Scale at pxyz[%+9.6f, %+9.6f, %+9.6f]\n", pxyz[0], pxyz[1], pxyz[2]);
#endif
            for(long i = 0; i < n; i++)
            {
                offset = i * TpslmrXform.COEFF_SIZE;

                dist = Math.sqrt(
                           Math.pow((double)((coeffs[offset + 0] / span) - pxyz[0]), 2) + 
                           Math.pow((double)((coeffs[offset + 1] / span) - pxyz[1]), 2) + 
                           Math.pow((double)((coeffs[offset + 2] / span) - pxyz[2]), 2));
                scale = tps_base(dist);
                sum[0] += wx[i] * scale;
                sum[1] += wy[i] * scale;
                sum[2] += wz[i] * scale;
#if DEBUG_XFORM
                stdout.printf("\tcoeff[%+11.6f, %+11.6f, %+11.6f], txyz[%+9.6f, %+9.6f, %+9.6f], dist[%+9.6f], span[%+9.6f] = %+9.6f\n", coeffs[offset+0], coeffs[offset+1], coeffs[offset+2], coeffs[offset+0]/span, coeffs[offset+1]/span, coeffs[offset+2]/span, dist, span, scale);
#endif
            }

            point.x = (pxyz[0] + (float)sum[0]) * span;
            point.y = (pxyz[1] + (float)sum[1]) * span;
            point.z = (pxyz[2] + (float)sum[2]) * span;

#if DEBUG_XFORM

            stdout.printf("Original point: \n%s\n", ori_lms.landmarks[pn].to_string());
            stdout.printf("Warped point: \n%s\n", point.to_string());
            stdout.printf("Target point: \n%s\n", this.group.target_landmarks.landmarks[pn].to_string());
#endif

            tmp_lms.landmarks[pn] = point;
        }

        this.group.temp_landmarks = tmp_lms;
    }

    public void warp()
    {
#if DEBUG_XFORM
        stdout.printf("Warping volume and landmarks\n");
#endif

        this.warp_landmarks();

#if USE_PHI
        this.group.original_volume.phi_alloc();
#endif
        warp_volume((float*)this.group.temp_volume.data, this.group.original_volume.data, this.tps.wx, this.tps.wy, this.tps.wz, this.coeffs, this.num_coeffs, this.max_d, this.dim, this.offset, this.spacing);
    }

    public void write(string output_fn)
    {
#if DEBUG_XFORM
        stdout.printf("TpslmrXform.write\n");
#endif

        var ios = FileStream.open(output_fn, "w");

        if(ios == null)
        {
            stdout.printf("Unable to write to file.\n");
            return;
        }

        var str = this.to_string();

#if DEBUG_XFORM
        stdout.printf("Data to write (%d bytes):\n%s\n", str.data.length, str);
#endif

        ios.write(str.data);
    }

    public void add_coeff(string name, float x, float y, float z, float dx, float dy, float dz)
    {
#if DEBUG_XFORM
        stdout.printf("TpslmrXform.add_coeff\n");
#endif
        this.coeffs = realloc(this.coeffs, sizeof(float) * TpslmrXform.COEFF_SIZE * (this.num_coeffs + 1));
        long offset = this.num_coeffs * TpslmrXform.COEFF_SIZE;
        this.coeffs[offset + 0] = x;
        this.coeffs[offset + 1] = y;
        this.coeffs[offset + 2] = z;
        this.coeffs[offset + 3] = dx;
        this.coeffs[offset + 4] = dy;
        this.coeffs[offset + 5] = dz;
        this.coeff_name.resize((int)(this.num_coeffs+1));
        this.coeff_name[this.num_coeffs] = name;
        this.num_coeffs++;
    }

    public void rem_coeffs(long idx)
    {
#if DEBUG
        stdout.printf("TpslmrXform.rem_coeffs\n");
#endif
        assert(idx > -1);
        assert(idx < this.num_coeffs);

        long d_idx = idx; //the coefficient set that will be replaced
        long s_idx = this.num_coeffs - 1; //the coefficient set that will replace the removed coefficient

        d_idx *= TpslmrXform.COEFF_SIZE;
        s_idx *= TpslmrXform.COEFF_SIZE;

        this.num_coeffs--;

        if(d_idx != s_idx) //if the coefficient being removed isn't the last one. 
        {
            for(long i = 0; i < TpslmrXform.COEFF_SIZE; i++) this.coeffs[d_idx + i] = this.coeffs[s_idx + i];
            this.coeff_name[d_idx] = this.coeff_name[s_idx];
        }

        this.coeffs = realloc(this.coeffs, sizeof(float) * TpslmrXform.COEFF_SIZE * this.num_coeffs);
        this.coeff_name.resize((int)this.num_coeffs);
    }

    public bool update_tps()
    {
        float[,] t_coeffs = new float[this.num_coeffs, TpslmrXform.COEFF_SIZE];

        for(long y = 0; y < this.num_coeffs; y++)
        {
            long offset = y * TpslmrXform.COEFF_SIZE;
            for(long x = 0; x < TpslmrXform.COEFF_SIZE; x++)
            {
                t_coeffs[y,x] = this.coeffs[offset + x];
            }
        }

        return this.tps.set_control_points(t_coeffs);
    }

    public void xform_point(float xi, float yi, float zi, out float xo, out float yo, out float zo)
    {
        double sum[3] = {0, 0, 0};
        long dim[3] = {0, 0, 0};
        for(long i = 0; i < 3; i++) dim[i] = this.group.original_volume.dim[i];
        float pos[3] = {xi, yi, zi};
        float pxyz[3] = {pos[0] / dim[0], pos[1] / dim[1], pos[2] / dim[2]};
        long n = this.num_coeffs;

        var wx = this.tps.wx;
        var wy = this.tps.wy;
        var wz = this.tps.wz;

        sum[0] += wx[n+0];
        sum[1] += wy[n+0];
        sum[2] += wz[n+0];
        sum[0] += wx[n+1] * ((double)(pxyz[0]));
        sum[1] += wy[n+1] * ((double)(pxyz[0]));
        sum[2] += wz[n+1] * ((double)(pxyz[0]));
        sum[0] += wx[n+2] * ((double)(pxyz[1]));
        sum[1] += wy[n+2] * ((double)(pxyz[1]));
        sum[2] += wz[n+2] * ((double)(pxyz[1]));
        sum[0] += wx[n+3] * ((double)(pxyz[2]));
        sum[1] += wy[n+3] * ((double)(pxyz[2]));
        sum[2] += wz[n+3] * ((double)(pxyz[2]));

        for(long i = 0; i < n; i++)
        {
            long offset = i * TpslmrXform.COEFF_SIZE;
            double scale = Math.sqrt(
                    Math.pow((double)((this.coeffs[offset + 0] / dim[0]) - pxyz[0]), 2) + 
                    Math.pow((double)((this.coeffs[offset + 1] / dim[1]) - pxyz[1]), 2) + 
                    Math.pow((double)((this.coeffs[offset + 2] / dim[2]) - pxyz[2]), 2));
            scale = tps_base(scale);
            sum[0] += wx[i] * scale;
            sum[1] += wy[i] * scale;
            sum[2] += wz[i] * scale;
        }

        xo = (pxyz[0] - (float)sum[0]) * dim[0];
        yo = (pxyz[1] - (float)sum[1]) * dim[1];
        zo = (pxyz[2] - (float)sum[2]) * dim[2];
    }

    [CCode(cheader_filename="tps_diff.h", cname="tps_rbf3")]
    private extern static double tps_base(double r);
}
