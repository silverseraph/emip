#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <glib.h>

#ifndef __grid_helper_header__
#define __grid_helper_header__

const long int grid_edge_table[256];

const long int grid_tri_table[256][16];

#endif
