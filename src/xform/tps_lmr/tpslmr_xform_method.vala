public class TpslmrXformMethod : Object, XformMethod
{
    public MatchMethod match_method;
    public IsoMethod iso_method;
    public float iso_range_min;
    public float iso_range_max;
    public long pr_dim[3];
    public float decimate;
    public float regularize;
    public Grid target_grid;
    public Grid original_grid;

    [CCode(cheader_filename="tps_diff.h", cname="dist3df")]
    public static extern float dist3df(float x, float y, float z, float a, float b, float c);

    public TpslmrXformMethod()
    {
        this.match_method = MatchMethod.PR;
        this.iso_method = IsoMethod.RANGE;
        this.iso_range_min = 0f;
        this.iso_range_max = 1f;
        this.decimate = 0.1f;
        this.regularize = 0f;
        for(long d = 0; d < 3; d++)
        {
            pr_dim[d] = 10;
        }
    }

    public bool handle_arg(string[] args, ref int i)
    {
        string arg = null;
        switch(args[i])
        {
            case "--isomethod":
                i++;
                if(i < args.length)
                {
                    arg = args[i];
                    if(arg.down() == "range")
                    {
                        this.iso_method = IsoMethod.RANGE;
                    }
                    else if(arg.down() == "canny")
                    {
                        this.iso_method = IsoMethod.CANNY;
                    }
                    else
                    {
                        this.iso_method = IsoMethod.RANGE;
                        log_warn("IsoMethod \"%s\" not recognized. Defaulting to RANGE\n", arg);
                    }
                }
                else
                {
                    log_warn("No option provided to %s.  Skipping...\n", args[i-1]);
                }
                return true;
            case "--threshold":
                i++;
                if(i < args.length)
                {
                    if(2 == args[i].scanf("%g:%g", &this.iso_range_min, &this.iso_range_max))
                    {
                        if(this.iso_range_min < 0 || this.iso_range_min > 1)
                        {
                            log_warn("The minimum value for the range must be in the range [0,1]\n");
                            this.iso_range_min = 0;
                        }

                        if(this.iso_range_max > 1 || this.iso_range_max < 0)
                        {
                            log_warn("The maximum value for the range must be in the range [0,1]\n");
                            this.iso_range_max = 1;
                        }

                        if(this.iso_range_max <= this.iso_range_min)
                        {
                            log_warn("The maximum value must be greater than the minimum value.  Defaulting to 0.5:1.0\n");
                            this.iso_range_min = 0.5f;
                            this.iso_range_max = 1.0f;
                        }
                    }
                    else
                    {
                        this.iso_range_min = 0.5f;
                        this.iso_range_max = 1.0f;
                        log_err("Ill formatted option provided to \"--threshold\" argument. Defaulting to [0.5,1.0]\n");
                    }
                }
                else
                {
                    log_warn("No option provided to %s.  Skipping...\n", args[i-1]);
                }
                return true;
            case "--griddim":
                i++;
                if(i < args.length)
                {
                    arg = args[i];

                    long pr_dim_tmp[3] = {0, 0, 0};

                    if(3 == arg.scanf("%ld,%ld,%ld", &pr_dim_tmp[0], &pr_dim_tmp[1], &pr_dim_tmp[2]))
                    {
                        this.pr_dim[0] = pr_dim_tmp[0];
                        this.pr_dim[1] = pr_dim_tmp[1];
                        this.pr_dim[2] = pr_dim_tmp[2];
                    }
                    else
                    {
                        log_warn("Failed to match dimension specifier \"%%ld,%%ld,%%ld\" with option \"%s\".  Will not set values.\n", arg);
                    }
                }
                else
                {
                    log_warn("No option provided to %s.  Skipping...\n", args[i-1]);
                }
                return true;
            case "--decimate":
                i++;
                if(i < args.length)
                {
                    arg = args[i];

                    float decimate_tmp = 0f;

                    if(1 == arg.scanf("%f", &decimate_tmp))
                    {
                        this.decimate = decimate_tmp;
                    }
                    else
                    {
                        log_warn("Failed to match decimation specifier \"%%f\" with option \"%s\".  Will not set value.\n", arg);
                    }
                }
                else
                {
                    log_warn("No option provided to %s.  Skipping...\n", args[i-1]);
                }
                return true;
            case "--match-method":
                i++;
                if(i < args.length)
                {
                    arg = args[i];

                    if("pattern-rec" == arg)
                    {
                        this.match_method = MatchMethod.PR;
                    }
                    else if("mesh-match" == arg)
                    {
                        this.match_method = MatchMethod.MESH;
                    }
                    else
                    {
                        log_warn("Failed to provide a valid match method with option, \"%s\".  Will not set value.\n", arg);
                    }
                }
                else
                {
                    log_warn("No option provided to %s.  Skipping...\n", args[i-1]);
                }
                return true;
            case "--regularize":
                i++;
                if(i < args.length)
                {
                    arg = args[i];

                    float reg = 0f;

                    if(1 == arg.scanf("%f", &reg))
                    {
                        this.regularize = reg;
                    }
                    else
                    {
                        log_warn("Failed to match regularize specifier \"%%f\" with option \"%s\".  Will not set value.\n", args[i-1]);
                    }
                }
                else
                {
                    log_warn("No option provided to %s.  Skipping...\n", args[i-1]);
                }
                return true;
            default:
                log_warn("Unhandled argument: \"%s\".  Continuing...\n", args[i]);
                return false;
        }
    }

    public string get_name() 
    { 
        const string mod_name = "tpslmr";
        return mod_name.dup(); 
    }

    public bool requires_landmarks() { return false; }

    public void configure(RegGroup group)
    {
        //grids
        //this.target_grid = new Grid(this.pr_dim[0], this.pr_dim[1], this.pr_dim[2]);
        //this.target_grid.init_grid();
        //this.target_grid.build_isomesh(group.target_volume, this.iso_range_min);

        //this.original_grid = new Grid(this.pr_dim[0], this.pr_dim[1], this.pr_dim[2]);
        //this.original_grid.init_grid();
        //optimizer
        group.optimizer = new TpslmrOptimizer(group);
        //volumes
        group.temp_volume = group.original_volume.full_clone();
        group.best_volume = group.original_volume.full_clone();

        //find the matches using PR method
        float[,,,] score_ta = new float[pr_dim[0], pr_dim[1], pr_dim[2], 3]; //target
        float[,,,] score_oi = new float[pr_dim[0], pr_dim[1], pr_dim[2], 3]; //original
        float offset[3] = {0, 0, 0};  
        float spacing[3] = {0, 0, 0}; 
        long dim[3] = {0, 0, 0};
        long dim_span[3] = {0, 0, 0};
        long dim_loc[3] = {0, 0, 0};
        for(long i = 0; i < 3; i++) 
        {
            dim[i] = group.original_volume.dim[i];
            dim_span[i] = (dim[i] / pr_dim[i]) + (((dim[i] % pr_dim[i]) > 0) ? 1 : 0);
            offset[i] = group.original_volume.offset[i];
            spacing[i] = group.original_volume.spacing[i];
        }

        //calculate volumetric mean and variance proto-values
        for(long x = 0; x < dim[0]; x++)
        {
            dim_loc[0] = x / dim_span[0];
            for(long y = 0; y < dim[1]; y++)
            {
                dim_loc[1] = y / dim_span[1];
                for(long z = 0; z < dim[2]; z++)
                {
                    dim_loc[2] = z / dim_span[2];

                    score_ta[dim_loc[0], dim_loc[1], dim_loc[2], 0] += group.target_volume.data[x, y, z];
                    score_ta[dim_loc[0], dim_loc[1], dim_loc[2], 1] += group.target_volume.data[x, y, z] * group.target_volume.data[x, y, z];
                    score_ta[dim_loc[0], dim_loc[1], dim_loc[2], 2] += 1;
                    score_oi[dim_loc[0], dim_loc[1], dim_loc[2], 0] += group.original_volume.data[x, y, z];
                    score_oi[dim_loc[0], dim_loc[1], dim_loc[2], 1] += group.original_volume.data[x, y, z] * group.original_volume.data[x, y, z];
                    score_oi[dim_loc[0], dim_loc[1], dim_loc[2], 2] += 1;
                }
            }
        }

        //finalize mean + variance
        for(long x = 0; x < pr_dim[0]; x++)
        {
            for(long y = 0; y < pr_dim[1]; y++)
            {
                for(long z = 0; z < pr_dim[2]; z++)
                {
                    score_ta[x, y, z, 0] /= score_ta[x, y, z, 2];
                    score_ta[x, y, z, 1] = (score_ta[x, y, z, 1] / score_ta[x, y, z, 2]) - Math.powf(score_ta[x, y, z, 0], 2);
                    score_oi[x, y, z, 0] /= score_oi[x, y, z, 2];
                    score_oi[x, y, z, 1] = (score_oi[x, y, z, 1] / score_oi[x, y, z, 2]) - Math.powf(score_oi[x, y, z, 0], 2);

                    log_infos("feature at [%ld,%ld,%ld] -> tar[%g, %g, %g], ori[%g, %g, %g]\n", x, y, z, score_ta[x, y, z, 0], score_ta[x, y, z, 1], score_ta[x, y, z, 2], score_oi[x, y, z, 0], score_oi[x, y, z, 1], score_oi[x, y, z, 2]);
                }
            }
        }

        debug_wait();

        float max_dist = dist3df(
                                offset[0], 
                                offset[1], 
                                offset[2], 
                                offset[0] + (float)dim[0] * spacing[0], 
                                offset[1] + (float)dim[1] * spacing[1], 
                                offset[2] + (float)dim[2] * spacing[2]
                                );
        long best_match[3] = {0, 0, 0};
        float best_score = float.MIN;
        float xyzp[3] = {0, 0, 0};
        float abcp[3] = {0, 0, 0};
        long idx = 0;

        for(long x = 0; x < pr_dim[0]; x++)
        {
            xyzp[0] = offset[0] + ((((float)x) + 0.5f) * dim_span[0]) * spacing[0];
            for(long y = 0; y < pr_dim[1]; y++)
            {
                xyzp[1] = offset[1] + ((((float)y) + 0.5f) * dim_span[1]) * spacing[1];
                for(long z = 0; z < pr_dim[2]; z++)
                {
                    xyzp[2] = offset[2] + ((((float)z) + 0.5f) * dim_span[2]) * spacing[2];
                    best_score = float.MAX;
                    float a_mean = score_ta[x, y, z, 0];
                    float a_var  = score_ta[x, y, z, 1];
                    for(long a = 0; a < pr_dim[0]; a++)
                    { 
                        abcp[0] = offset[0] + ((((float)a) + 0.5f) * dim_span[0]) * spacing[0];
                        for(long b = 0; b < pr_dim[1]; b++) 
                        { 
                            abcp[1] = offset[1] + ((((float)b) + 0.5f) * dim_span[1]) * spacing[1];
                            for(long c = 0; c < pr_dim[2]; c++)
                            {
                                abcp[2] = offset[2] + ((((float)c) + 0.5f) * dim_span[2]) * spacing[2];
                                float b_mean = score_oi[a, b, c, 0];
                                float b_var  = score_oi[a, b, c, 1];
                                float dist   = dist3df(xyzp[0], xyzp[1], xyzp[2], abcp[0], abcp[1], abcp[2]);
                                float score  = match_value(dist, max_dist, a_mean, a_var, b_mean, b_var);
                                if(score < best_score)
                                {
                                    best_score = score;
                                    best_match[0] = a;
                                    best_match[1] = b;
                                    best_match[2] = c;
                                }
                            }
                        }
                    }

#if DEBUG_OPTIMIZER
                    stdout.printf("Linking [%ld, %ld, %ld]:[%f, %f, %f] to [%ld, %ld, %ld]:[%f, %f, %f]\n", 
                        x, y, z, 
                        offset[0] + ((((float)x) + 0.5f) * dim_span[0]) * spacing[0], offset[1] + ((((float)y) + 0.5f) * dim_span[1]) * spacing[1], offset[2] + ((((float)z) + 0.5f) * dim_span[2]) * spacing[2], 
                        best_match[0], best_match[1], best_match[2], 
                        offset[0] + ((((float)best_match[0]) + 0.5f) * dim_span[0]) * spacing[0], offset[1] + ((((float)best_match[1]) + 0.5f) * dim_span[1]) * spacing[1], offset[2] + ((((float)best_match[2]) + 0.5f) * dim_span[2]) * spacing[2]
                        );
#endif

                    group.original_landmarks.add_point("dyn-%ld".printf(idx), offset[0] + ((float)(x * dim_span[0]) * spacing[0]), offset[1] + ((float)(y * dim_span[1]) * spacing[1]), offset[2] + ((float)(z * dim_span[2]) * spacing[2]));
                    group.target_landmarks.add_point("dyn-%ld".printf(idx), offset[0] + ((float)(best_match[0] * dim_span[0]) * spacing[0]), offset[1] + ((float)(best_match[1] * dim_span[1]) * spacing[1]), offset[2] + ((float)(best_match[2] * dim_span[2]) * spacing[2]));

                    idx++;
                }
            }
        }
            
        var xform = new TpslmrXform.as_original(group, this);

#if DEBUG_OPTIMIZER
        xform.print();
#endif

        group.temp_xform = xform.clone();
        group.best_xform = xform.clone();
    }

    [CCode(cheader_filename="tps_diff.h", cname="match_value")]
    public static extern float match_value(float dist, float max_dist, float a_mean, float a_var, float b_mean, float b_var);

    public string method_help()
    {
        return ("Xform usage: %s\n" +
                "\t--isomethod    [range | canny]            Set the iso-method to range or canny.\n" +
                "\t--threshold    [min_val:max_val]          Set the post iso-method filter values.\n" +
                "\t--griddim      [x,y,z]                    Set the grid dimensions for PR or isomesh.\n" +
                "\t--decimate     [percent]                  Remove vertices and edges on the isomesh down to \"percent\"\n" +
                "\t--regularize   [percent]                  Regularize the TPS matrix, which gives smoothed deformation\n" +
                "\t--match-method [pattern-rec | mesh-match] Select optimization between pattern recognition or mesh matching\n").printf(this.get_name());
    }
}

public enum MatchMethod
{
    PR,
    MESH
}

public enum IsoMethod
{
    RANGE,
    CANNY
}
