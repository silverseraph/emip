using Gsl;

public class Tps
{
    public const long COEFF_SIZE = 6; //x, y, z, m

    public double* wx;
    public double* wy;
    public double* wz;

    public float[,] coeffs;

    /* maximum coordinate delta */
    public float max_real;
    public float reg;

    public Tps(float max_real, float reg = 0)
    {
        this.coeffs = new float[0,COEFF_SIZE];
        this.wx = null;
        this.wy = null;
        this.wz = null;
        this.max_real = max_real;
        this.reg = reg;
    }

    [CCode(cheader_filename="tps_diff.h", cname="tps_rbf3")]
    private extern static double tps_base(double r);

    [CCode(cheader_filename="tps_diff.h", cname="dist3d")]
    private extern static double dist3d(double x, double y, double z, double a, double b, double c);

    public bool set_control_points(float[,] coeffs)
    {
        assert(coeffs.length[1] == COEFF_SIZE);

        if(coeffs.length[0] < 4)
        {
            this.wx = new double[coeffs.length[0] + 4];
            this.wy = new double[coeffs.length[0] + 4];
            this.wz = new double[coeffs.length[0] + 4];
            return false;
        }

        this.coeffs = new float[coeffs.length[0], coeffs.length[1]];

        //apply the scaling factor, assuming that not every point is the same.
        for(long i = 0; i < coeffs.length[0]; i++) 
        {
            for(long j = 0; j < coeffs.length[1]; j++) 
            {
                this.coeffs[i,j] = coeffs[i,j] / this.max_real;
            }
        }

#if DEBUG_TPS
        stdout.printf("Will use control points for tps:\n");
        stdout.printf("max_real value: %f\n", this.max_real);
        stdout.printf("%-10s %-10s %-10s %-10s %-10s %-10s\n", "x", "y", "z", "dx", "dy", "dz");
        for(long i = 0; i < this.coeffs.length[0]; i++)
        {
            stdout.printf("%+9.6f\t%+9.6f\t%+9.6f\t%+9.6f\t%+9.6f\t%+9.6f\n", this.coeffs[i, 0], this.coeffs[i, 1], this.coeffs[i, 2], this.coeffs[i, 3], this.coeffs[i, 4], this.coeffs[i, 5]);
        }
#endif

        long n = this.coeffs.length[0];

        double[,] mat_x = new double[n + 4, n + 4];
        double[,] mat_y = new double[n + 4, n + 4];
        double[,] mat_z = new double[n + 4, n + 4];

        double reg_sum = 0.0, val = 0.0, a = 0.0;

        //fill K
        for(long x = 0; x < n; x++)
        {
            for(long y = x; y < n; y++)
            {
                val = tps_base(dist3d(this.coeffs[x,0], this.coeffs[x,1], this.coeffs[x,2],
                                      this.coeffs[y,0], this.coeffs[y,1], this.coeffs[y,2]));
                mat_x[x,y] = mat_x[y,x] = val;
                mat_y[x,y] = mat_y[y,x] = val;
                mat_z[x,y] = mat_z[y,x] = val;
                reg_sum += val;
            }
        }

        a = (reg_sum * 2) / (n * n);

        //fill P and P^t
        for(long i = 0; i < n; i++)
        {
            mat_x[n,n] = mat_y[n,n] = mat_z[n,n] = this.reg * a * a;
            //row[i], col[n+0:n+3]
            mat_x[i,n+0] = mat_y[i,n+0] = mat_z[i,n+0] = (double)1;
            mat_x[i,n+1] = mat_y[i,n+1] = mat_z[i,n+1] = (double)this.coeffs[i, 0];
            mat_x[i,n+2] = mat_y[i,n+2] = mat_z[i,n+2] = (double)this.coeffs[i, 1];
            mat_x[i,n+3] = mat_y[i,n+3] = mat_z[i,n+3] = (double)this.coeffs[i, 2];

            //col[i], row[n+0:n+3]
            mat_x[n+0,i] = mat_y[n+0,i] = mat_z[n+0,i] = (double)1;
            mat_x[n+1,i] = mat_y[n+1,i] = mat_z[n+1,i] = (double)this.coeffs[i, 0];
            mat_x[n+2,i] = mat_y[n+2,i] = mat_z[n+2,i] = (double)this.coeffs[i, 1];
            mat_x[n+3,i] = mat_y[n+3,i] = mat_z[n+3,i] = (double)this.coeffs[i, 2];
        }

        //fill O
        for(long x = n; x < (n+4); x++)
        {
            for(long y = n; y < (n+4); y++)
            {
                mat_x[x,y] = mat_y[x,y] = mat_z[x,y] = (double)0;
            }
        }

        //fill diagonal as zeros
        for(long x = 0; x < (n+4); x++)
        {
            mat_x[x,x] = mat_y[x,x] = mat_z[x,x] = (double)0;
        }

        var mv_xt = new double[(n+4)*(n+4)];
        var mv_yt = new double[(n+4)*(n+4)];
        var mv_zt = new double[(n+4)*(n+4)];

        for(long y = 0; y < (n+4); y++)
        {
            for(long x = 0; x < (n+4); x++)
            {
                long offset = (y * (n+4)) + x;
                mv_xt[offset] = mat_x[x, y];
                mv_yt[offset] = mat_y[x, y];
                mv_zt[offset] = mat_z[x, y];
            }
        }

#if DEBUG_TPS
        stdout.printf("tps matrix:\n");
        print_matrix(mat_x);
#endif

        var mv_x = MatrixView.array(mv_xt, (n+4), (n+4));
        var mv_y = MatrixView.array(mv_yt, (n+4), (n+4));
        var mv_z = MatrixView.array(mv_zt, (n+4), (n+4));

        //weight vector
        var vec_x = new double[n + 4];
        var vec_y = new double[n + 4];
        var vec_z = new double[n + 4];
        for(long y = 0; y < n; y++)
        {
            vec_x[y] = (double)this.coeffs[y,3];
            vec_y[y] = (double)this.coeffs[y,4];
            vec_z[y] = (double)this.coeffs[y,5];
        }
        for(long y = n; y < (n+4); y++) vec_x[y] = vec_y[y] = vec_z[y] = (double)0;

        var vv_x = VectorView.array(vec_x);
        var vv_y = VectorView.array(vec_y);
        var vv_z = VectorView.array(vec_z);

        //setup for GSL
        Vector res_x = new Vector(n+4);
        Vector res_y = new Vector(n+4);
        Vector res_z = new Vector(n+4);
        int s[3];
        Permutation p[3] = {new Permutation(n+4), new Permutation(n+4), new Permutation(n+4)};

        //run LU decomp to solve for w and a
        LinAlg.LU_decomp((Matrix)(&mv_x.matrix), p[0], out s[0]);
        LinAlg.LU_decomp((Matrix)(&mv_y.matrix), p[1], out s[1]);
        LinAlg.LU_decomp((Matrix)(&mv_z.matrix), p[2], out s[2]);

        LinAlg.LU_solve((Matrix)(&mv_x.matrix), p[0], (Vector)(&vv_x.vector), res_x);
        LinAlg.LU_solve((Matrix)(&mv_y.matrix), p[1], (Vector)(&vv_y.vector), res_y);
        LinAlg.LU_solve((Matrix)(&mv_z.matrix), p[2], (Vector)(&vv_z.vector), res_z);

        if(this.wx != null) free(this.wx);
        if(this.wy != null) free(this.wy);
        if(this.wz != null) free(this.wz);

        this.wx = new double[n+4];
        this.wy = new double[n+4];
        this.wz = new double[n+4];

#if DEBUG_TPS
        stdout.printf("Tps Weights:\n%-10s\t%-10s\t%-10s\n", " x", " y", " z");
#endif

        for(long i = 0; i < n + 4; i++)
        {
            this.wx[i] = res_x.ptr(i)[0];
            this.wy[i] = res_y.ptr(i)[0];
            this.wz[i] = res_z.ptr(i)[0];

#if DEBUG_TPS
            stdout.printf("%+9.6f\t%+9.6f\t%+9.6f\n", this.wx[i], this.wy[i], this.wz[i]);
#endif
        }

        return true;
    }

#if DEBUG_TPS
    private static void print_matrix(double[,] m)
    {
        for(long y = 0; y < m.length[1]; y++)
        {
            for(long x = 0; x < m.length[0]; x++)
            {
                stdout.printf("%+9.6f ", m[x,y]);
            }
            stdout.putc('\n');
        }
    }
#endif
}
