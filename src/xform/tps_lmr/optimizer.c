#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

#if defined(_WIN32) || defined(WIN32)
#define <windows.h>
#include <io.h>
#else
#include <unistd.h>
#endif

#include <omp.h>
#include <glib.h>

#include "../../emip_utils.h"

typedef struct Landmarks Landmarks;

bool landmarks_add_point(Landmarks *this, char *name, float x, float y, float z, float weight, bool selected, bool visible);

void create_mappings(Landmarks *a, Landmarks *b, float *vol_a, float *vol_b, float *spacing, float *offset, glong *dim, glong *pr_dim)
{
    const glong n_features = 2;
    glong *i_dim = malloc(sizeof(glong) * 4);
    glong len = pr_dim[0] * pr_dim[1] * pr_dim[2];
    float *pr_a = malloc(sizeof(float) * len * 3);
    float *pr_b = malloc(sizeof(float) * len * 3);
    glong idx = 0;
    glong z, y, x;
    i_dim[0] = dim[0];
    i_dim[1] = dim[1];
    i_dim[2] = dim[2];
    i_dim[3] = n_features;
}
