#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#if FOUND_OMP
#include <omp.h>
#endif
#include <glib.h>

#include "../../fastonebigheader.h"
#include "tps_diff.h"
#include "../../emip_utils.h"

PHI_FUNC double dist3d(double x, double y, double z, double a, double b, double c)
{
    return sqrt(
            pow(x - a, 2) +
            pow(y - b, 2) +
            pow(z - c, 2));
}

PHI_FUNC float dist3df(float x, float y, float z, float a, float b, float c)
{
    return sqrtf(
            powf(x - a, 2) +
            powf(y - b, 2) +
            powf(z - c, 2));
}

#ifdef USE_OMP
#pragma omp declare simd
#endif
PHI_FUNC static inline float _dist3df(float x, float y, float z, float a, float b, float c)
{
    return sqrtf((x-a)*(x-a) + (y-b)*(y-b) + (z-c)*(z-c));
}

PHI_FUNC float match_value(float dist, float max_dist, float a_mean, float a_var, float b_mean, float b_var)
{
    const float dist_weight = 5;
    const float mean_weight = 2;
    const float  var_weight = 0;

    return ((dist_weight * (dist / max_dist)) +
            (mean_weight * (fabsf(a_mean - b_mean))) +
            ( var_weight * (fabsf( a_var -  b_var))));
}

PHI_FUNC double tps_rbf3(double r)
{
    if(r < 0.00001) return 0;
#if defined(USE_SYSTEM_MATH) || defined(USE_PHI) || defined(USE_ICC)
    return r * r * log(r);
#else
    return r * r * fastlog(r);
#endif
}

#ifdef USE_OMP
#pragma omp declare simd
#endif
PHI_FUNC static inline float _tps_rbf3(float r)
{
    if(r < 0.000001) return 0;
#if defined(USE_SYSTEM_MATH) || defined(USE_PHI) || defined(USE_ICC)
    return r * r * logf(r);
#else
    return r * r * fastlog(r);
#endif
}

float tps_mse(long int dim_x, long int dim_y, long int dim_z, float *da, float *db)
{
    long int len = dim_x * dim_y * dim_z;
    long int idx = 0;
    long int start, end;
    int pid = 0;
    float err_loc = 0;
    float err_sum = 0;
    float *err_sums = (float*)malloc(sizeof(float) * dim_x);
    GTimer *t = g_timer_new();

    g_timer_start(t);

#ifdef USE_PHI
#pragma offload target(mic) \
    in(da : length(0) REUSE) \
    in(db : length(0) REUSE) \
    out(err_sums : length(dim_x))
#endif
    {
#ifdef USE_OMP
        #pragma omp parallel for \
            default(none) \
            private(idx, err_loc, pid, start, end) \
            shared(dim_x, dim_y, dim_z, da, db, err_sums)
#endif
        for(pid = 0; pid < dim_x; pid++)
        {
            err_loc = 0;
            start = (pid + 0) * (dim_y * dim_z);
            end = (pid + 1) * (dim_y * dim_z);
            #pragma omp simd reduction(+:err_loc)
            for(idx = start; idx < end; idx++)
            {
                //err_loc += powf(da[idx] - db[idx], 2.0);
                err_loc += (da[idx] - db[idx]) * (da[idx] - db[idx]);
            }

            err_sums[pid] = err_loc;
        }
    }

    fprintf(stderr, "MSE runtime <" OMP_STR " " PHI_STR ">: %g\n", g_timer_elapsed(t, NULL));

    g_timer_destroy(t);

    for(idx = 0; idx < dim_x; idx++) err_sum += err_sums[idx];

    return err_sum / len;
}

void warp_volume(float *vol_data_out, float *vol_data_in, double *wx, double *wy, double *wz, float *coeffs, glong num_coeffs, float max_d, glong *a_dim, float *a_offset, float *a_spacing)
{
    float pxyz[3] = {0, 0, 0};
    float sxyz[3] = {0, 0, 0};
    long   dim[3] = {0, 0, 0};
    /*float sum[3] = {0, 0, 0};*/
    glong aloc[6] = {0, 0, 0, 0, 0, 0};
    glong stride[3] = {0, 0, 0};
    float d[6] = {0, 0, 0, 0, 0, 0};
    float origin[3] = {0, 0, 0};
    float spacing[3] = {0, 0, 0};
    float span = max_d;
    float  vox[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    glong n = num_coeffs;
    float voxel_avg = 0;
    float scale = 12345;
    float d2 = 0.0f;
    float s0 = 0.0f, s1 = 0.0f, s2 = 0.0f;
    glong x, y, z, lim, idx, i, new_idx;
    GTimer *timer;

    float *s_coeff_x = malloc(sizeof(float) * n);
    float *s_coeff_y = malloc(sizeof(float) * n);
    float *s_coeff_z = malloc(sizeof(float) * n);
    float *fwx       = malloc(sizeof(float) * (n+4));
    float *fwy       = malloc(sizeof(float) * (n+4));
    float *fwz       = malloc(sizeof(float) * (n+4));

#if USE_OMP
#pragma omp parallel for simd
#endif
    for(glong i = 0; i < n; i++)
    {
        s_coeff_x[i] = coeffs[(i*6)+0] / span;
        s_coeff_y[i] = coeffs[(i*6)+1] / span;
        s_coeff_z[i] = coeffs[(i*6)+2] / span;
    }

#if USE_OMP
#pragma omp parallel for simd
#endif
    for(glong i = 0; i < (n+4); i++)
    {
        fwx[i] = (float)wx[i];
        fwy[i] = (float)wy[i];
        fwz[i] = (float)wz[i];
    }

    timer = g_timer_new();

    for(glong i = 0; i < 3; i++)
    {
        dim[i] = a_dim[i];
        origin[i] = a_offset[i];
        spacing[i] = a_spacing[i];
    }

    stride[0] = dim[1] * dim[2];
    stride[1] = dim[2];
    stride[2] = 1;

    g_timer_start(timer);

    lim = dim[0] * dim[1] * dim[2];
#ifdef USE_PHI
#pragma offload target(mic) \
    in(vol_data_in : length(0) REUSE) \
    in(fwx,fwy,fwz:length(n+4)) \
    in(s_coeff_x:length(n)) \
    in(s_coeff_y:length(n)) \
    in(s_coeff_z:length(n)) \
    out(vol_data_out:length(lim))
#endif
    {
#ifdef USE_OMP
        #pragma omp parallel for \
            default(none) \
            schedule(auto) \
            private(i, idx, x, y, z, pxyz, sxyz, aloc, d, d2, s0, s1, s2, voxel_avg, vox, scale, new_idx) \
            shared(s_coeff_x, s_coeff_y, s_coeff_z, vol_data_in, vol_data_out, stride, fwx, fwy, fwz, lim, dim, spacing, origin, span, n)
#endif
        for(idx = 0; idx < lim; idx++)
        {
            z = idx % dim[2];
            y = (idx / dim[2]) % dim[1];
            x = idx / (stride[0]);

            pxyz[0] = (origin[0] + (spacing[0] * x)) / span;
            pxyz[1] = (origin[1] + (spacing[1] * y)) / span;
            pxyz[2] = (origin[2] + (spacing[2] * z)) / span;

            s0 = fwx[n+0]; 
            s1 = fwy[n+0]; 
            s2 = fwz[n+0];
            s0 += fwx[n+1] * pxyz[0];
            s1 += fwy[n+1] * pxyz[1];
            s2 += fwz[n+1] * pxyz[2];
            s0 += fwx[n+2] * pxyz[0];
            s1 += fwy[n+2] * pxyz[1];
            s2 += fwz[n+2] * pxyz[2];
            s0 += fwx[n+3] * pxyz[0];
            s1 += fwy[n+3] * pxyz[1];
            s2 += fwz[n+3] * pxyz[2];

#ifdef FOUND_OMP
            #pragma omp simd reduction(+:s0, s1, s2)
#endif
            for(i = 0; i < n; i++)
            {
                /*scale = _tps_rbf3(_dist3df(s_coeff_x[i], s_coeff_y[i], s_coeff_z[i], pxyz[0], pxyz[1], pxyz[2]));*/
                d2 = ((s_coeff_x[i] - pxyz[0])*(s_coeff_x[i]-pxyz[0])) + ((s_coeff_y[i] - pxyz[1])*(s_coeff_y[i] - pxyz[1])) + ((s_coeff_z[i] - pxyz[2])*(s_coeff_z[i] - pxyz[2]));

#if defined(USE_SYSTEM_MATH) || defined(USE_PHI) || defined(__INTEL_COMPILER) || defined(USE_ICC)
                scale = d2 * logf(sqrtf(d2)+0.00001f);
#else
                scale = d2 * fastlog(sqrtf(d2)+0.00001f);
#endif
                
                s0 += fwx[i] * scale;
                s1 += fwy[i] * scale;
                s2 += fwz[i] * scale;
            }

            sxyz[0] = (((pxyz[0] + s0) * span) - origin[0]) / spacing[0];
            sxyz[1] = (((pxyz[1] + s1) * span) - origin[1]) / spacing[1];
            sxyz[2] = (((pxyz[2] + s2) * span) - origin[2]) / spacing[2];

            if(G_UNLIKELY((sxyz[0] < 0) || (sxyz[1] < 0) || (sxyz[2] < 0) || (sxyz[0] >= dim[0]) || (sxyz[1] >= dim[1]) || (sxyz[2] >= dim[2])))
            {
                vol_data_out[idx] = 0;
            }
            else
            {
                voxel_avg = 0;

                aloc[0] = (glong)floorf(sxyz[0]);
                aloc[1] = (glong)floorf(sxyz[1]);
                aloc[2] = (glong)floorf(sxyz[2]);
                aloc[3] = aloc[0] + 1;
                aloc[4] = aloc[1] + 1;
                aloc[5] = aloc[2] + 1;

                d[0] = sxyz[0] - aloc[0];
                d[1] = sxyz[1] - aloc[1];
                d[2] = sxyz[2] - aloc[2];
                d[3] = aloc[3] - sxyz[0];
                d[4] = aloc[4] - sxyz[1];
                d[5] = aloc[5] - sxyz[2];

                new_idx = VOL_IDX(aloc[0], aloc[1], aloc[2], dim);

                vox[0] = vol_data_in[new_idx + stride[0] + stride[1] + stride[2]]; //vol[x+1,y+1,z+1]
                vox[1] = vol_data_in[new_idx + stride[0] + stride[1]            ]; //vol[x+1,y+1,z+0]
                vox[2] = vol_data_in[new_idx + stride[0]             + stride[2]]; //etc
                vox[3] = vol_data_in[new_idx + stride[0]                        ];
                vox[4] = vol_data_in[new_idx             + stride[1] + stride[2]];
                vox[5] = vol_data_in[new_idx             + stride[1]            ];
                vox[6] = vol_data_in[new_idx                         + stride[2]];
                vox[7] = vol_data_in[new_idx                                    ];

#if defined(__cilk) &&  defined(__INTEL_COMPILER) && 0
                vox[0:4] *= d[0];
                vox[4:4] *= d[3];
                vox[0:2] *= d[1];
                vox[2:2] *= d[4];
                vox[4:2] *= d[1];
                vox[6:2] *= d[4];
                vox[0:4:2] *= d[2];
                vox[1:4:2] *= d[5];
                vox[0:4] += vox[1:4:2];
                vox[0:2] += vox[2:2];
                voxel_avg  = vox[0] + vox[1];
#else
                voxel_avg += vox[0] * (d[0]*d[1]*d[2]);
                voxel_avg += vox[1] * (d[0]*d[1]*d[5]);
                voxel_avg += vox[2] * (d[0]*d[4]*d[2]);
                voxel_avg += vox[3] * (d[0]*d[4]*d[5]);
                voxel_avg += vox[4] * (d[3]*d[1]*d[2]);
                voxel_avg += vox[5] * (d[3]*d[1]*d[5]);
                voxel_avg += vox[6] * (d[3]*d[4]*d[2]);
                voxel_avg += vox[7] * (d[3]*d[4]*d[5]);
#endif

                vol_data_out[idx] = voxel_avg;
            }
        }
    }

    free(s_coeff_x);
    free(s_coeff_y);
    free(s_coeff_z);

    free(fwx);
    free(fwy);
    free(fwz);

    fprintf(stdout, "TPS Volume Warp <" PHI_STR " " OMP_STR ">: %f\n", g_timer_elapsed(timer, NULL));

    g_timer_destroy(timer);
}

void threshold_volume(float *vol_data_in, long int len, float min, float max)
{
    long int i;
#ifdef USE_PHI
    #pragma offload target(mic) \
        in(vol_data_in : length(0) REUSE)
#endif
    {
#ifdef USE_OMP
        #pragma omp parallel for \
            default(none) \
            private(i) \
            shared(vol_data_in, len, min, max)
#endif
        for(i = 0; i < len; i++)
        {
            //clip out the values outside of the "good" range
            if(vol_data_in[i] < min || vol_data_in[i] > max)
            {
                vol_data_in[i] = 0.0;
            }
        }
    }
}
