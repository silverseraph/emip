public class TpslmrOptimizer : Object, Optimizer
{
    public RegGroup group;

    public TpslmrOptimizer(RegGroup group)
    {
        this.group = group;
    }

    [CCode(cname="tps_mse", cheader_filename="tps_diff.h", has_target=false)]
    extern static float tps_mse_diff(long dim_x, long dim_y, long dim_z, float *da, float *db);

    public float score(ScoreType type)
    {
        float score = 0f;
        float tmp_score = 0f;

        long[] dim = new long[3];

        for(long d = 0; d < 3; d++) dim[d] = this.group.original_volume.dim[d];

#if DEBUG_OPTIMIZER
        stdout.printf("dim:\n\t[0]: %lld\n\t[1]: %lld\n\t[2]: %lld\n", dim[0], dim[1], dim[2]);
#endif

        if(type == ScoreType.INITIAL)
        {
#if DEBUG_OPTIMIZER
            stdout.printf("Scoring using initial.\n");
#endif

            tmp_score = 0f;
            var tar_lms = this.group.target_landmarks.landmarks;
            var ori_lms = this.group.original_landmarks.landmarks;

            float weight_sum = 0f;

            float lm_max_err = Math.sqrtf((float)(dim[0] * dim[0] + dim[1] * dim[1] + dim[2] * dim[2]));

            var keys = tar_lms.get_keys();

            if(keys.size > 0)
            {
                foreach(var pn in keys)
                {
                    var point_a = tar_lms[pn];
                    var point_b = ori_lms[pn];
                    assert(point_a != null);
                    assert(point_b != null);

#if DEBUG_OPTIMIZER
                    stdout.printf(point_a.to_string());
                    stdout.printf(point_b.to_string());
#endif

                    tmp_score += ((point_a.weight + point_b.weight)/2) * Math.sqrtf(
                            Math.powf(point_a.x - point_b.x, 2f) + 
                            Math.powf(point_a.y - point_b.y, 2f) +
                            Math.powf(point_a.z - point_b.z, 2f));

                    weight_sum += (point_a.weight + point_b.weight) / 2;
                }

                if(weight_sum < 0.000001)
                    tmp_score = 0;
                else
                    tmp_score /= (lm_max_err * weight_sum);

                stdout.printf("Initial Landmark score: %f\n", tmp_score);

                score += tmp_score;
            }

            this.group.target_volume.phi_alloc();
            this.group.original_volume.phi_alloc();

            tmp_score = tps_mse_diff(dim[0], dim[1], dim[2], (float*)this.group.target_volume.data, (float*)this.group.original_volume.data);

            stdout.printf("Initial diff score: %f\n", tmp_score);

            score += tmp_score;

            return score;
        }

        if((type & ScoreType.LANDMARKS) == ScoreType.LANDMARKS)
        {
#if DEBUG_OPTIMIZER
            stdout.printf("Scoring with landmarks\n");
#endif
            var target_lms = this.group.target_landmarks;
            var temp_lms = this.group.temp_landmarks;
            float weight_sum = 0f;
            float lm_max_err = Math.sqrtf((float)(dim[0] * dim[0] + dim[1] * dim[1] + dim[2] * dim[2]));
            tmp_score = 0f;

            var keys = target_lms.landmarks.get_keys();

            if(keys.size > 0)
            {
                foreach(var pn in keys)
                {
                    var point_a = target_lms.landmarks[pn];
                    assert(point_a != null);
                    var point_b = temp_lms.landmarks[pn];
                    assert(point_b != null);
                    tmp_score += ((point_a.weight + point_b.weight)/2) * Math.sqrtf(Math.powf(point_a.x - point_b.x, 2f) + Math.powf(point_a.y - point_b.y, 2f) + Math.powf(point_a.z - point_b.z, 2f));
                    weight_sum += (point_a.weight + point_b.weight)/2;
                }

                if(weight_sum < 0.0000001) 
                    tmp_score = 0;
                else
                    tmp_score /= (weight_sum * lm_max_err);

                stdout.printf("Landmark score: %f\n", tmp_score);

                score += tmp_score;
            }
            else
            {
                log_warn("No landmark score given.\n");
            }
        }

        if((type & ScoreType.DIFF) == ScoreType.DIFF)
        {
#if DEBUG_OPTIMIZER
            stdout.printf("Scoring with diff\n");
#endif

            this.group.target_volume.phi_alloc();
            this.group.temp_volume.phi_alloc();

            tmp_score = tps_mse_diff(dim[0], dim[1], dim[2], (float*)this.group.target_volume.data, (float*)this.group.temp_volume.data);

            stdout.printf("MSE score: %f\n", tmp_score);
            score += tmp_score;
        }

        return score;
    }

    public void improve()
    {
        //for each coefficient in the set
        for(long i = 0; i < this.group.temp_xform.num_coeffs; i++)
        {
            //get landmark difference between temp_landmarks and target_landmarks
            string name = this.group.temp_xform.coeff_name[i];
            if(name.has_prefix("temp-")) continue;
            var i_point = this.group.temp_landmarks.landmarks[name]; //intermediate (temp) point
            var t_point = this.group.target_landmarks.landmarks[name]; //target point

            if(i_point == null)
            {
                log_warn("Landmark[\"%s\"] does not exist in temp landmarks.  Continuing...\n", name);
                continue;
            }

            if(t_point == null)
            {
                log_warn("Landmark[\"%s\"] does not exist in target landmarks.  Continuing...\n", name);
                continue;
            }

            float dx = (t_point.x - i_point.x) / this.group.temp_xform.dim[0];
            float dy = (t_point.y - i_point.y) / this.group.temp_xform.dim[1];
            float dz = (t_point.z - i_point.z) / this.group.temp_xform.dim[2];

#if DEBUG_OPTIMIZER
            stdout.printf("Adjusting landmark[%s] by:\n\tdx: %9g\n\tdy: %9g\n\tdz: %9g\n", name, dx, dy, dz);
#endif

            //add difference to temp_xform
            this.group.temp_xform.coeffs[(i * TpslmrXform.COEFF_SIZE)+3] -= dx;
            this.group.temp_xform.coeffs[(i * TpslmrXform.COEFF_SIZE)+4] -= dy;
            this.group.temp_xform.coeffs[(i * TpslmrXform.COEFF_SIZE)+5] -= dz;
        }

        this.group.temp_xform.update_tps();
    }
}
