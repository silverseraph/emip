#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <omp.h>
#include <glib.h>

#include "../../emip_utils.h"

#ifndef __tps_diff_header__
#define __tps_diff_header__

#define voxel_gap (1000000)

PHI_FUNC double dist3d(double x, double y, double z, double a, double b, double c);

PHI_FUNC float dist3df(float x, float y, float z, float a, float b, float c);

PHI_FUNC float match_value(float dist, float max_dist, float a_mean, float a_var, float b_mean, float b_var);

PHI_FUNC double tps_rbf3(double r);

float tps_mse(long int dim_x, long int dim_y, long int dim_z, float *da, float *db);

void warp_volume(float *vol_data_out, float *vol_data_in, double *wx, double *wy, double *wz, float *coeffs, long int num_coeffs, float max_d, long int *a_dim, float *a_offset, float *a_spacing);

void threshold_volume(float *vol_data_in, long int len, float min, float max);

#endif
