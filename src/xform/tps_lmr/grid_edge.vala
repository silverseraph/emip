public class GridEdge
{
    public long idx;

    public GridEdge(long idx)
    {
        this.idx = idx;
    }

    public static uint hash(GridEdge a)
    {
        return (uint)a.idx;
    }

    public static bool equal(GridEdge a, GridEdge b)
    {
        return a.idx == b.idx;
    }
}
