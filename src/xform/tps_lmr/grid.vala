public class Grid
{
    [CCode(cheader_filename="grid_helper.h", cname="grid_edge_table")]
    public extern long edge_table[256];

    [CCode(cheader_filename="grid_helper.h", cname="grid_tri_table")]
    public extern long *tri_table[256];

    public Graph<GridVertex, GridEdge> g;
    public long gdim[3];
    
    public Grid(long x = 10, long y = 10, long z = 10)
    {
        this.gdim[0] = x;
        this.gdim[1] = y;
        this.gdim[2] = z;

        this.g = new Graph<GridVertex, GridEdge>((HashFunc)GridVertex.hash, (EqualFunc)GridVertex.equal, (HashFunc)GridEdge.hash, (EqualFunc)GridVertex.equal);
    }

    public void set_gdim(long x, long y, long z)
    {
        this.gdim[0] = x;
        this.gdim[1] = y;
        this.gdim[2] = z;

        this.clear_graph();
        this.init_grid();
    }

    //initialize the graph grid
    public void init_grid()
    {
        long sidx[3] = {0, 0, 0};

        for(long z = 0; z < this.gdim[2]; z++)
        {
            sidx[2] = z * 2;
            for(long y = 0; y < this.gdim[1]; y++)
            {
                sidx[1] = y * 2;
                for(long x = 0; x < this.gdim[0]; x++)
                {
                    sidx[0] = x * 2;

                    this.g.add_vertex(new  GridVertex(sidx[0] +  1, sidx[1] + 0, sidx[2]  + 0));
                    this.g.add_vertex(new  GridVertex(sidx[0] +  0, sidx[1] + 1, sidx[2]  + 0));
                    this.g.add_vertex(new  GridVertex(sidx[0] +  2, sidx[1] + 1, sidx[2]  + 0));
                    this.g.add_vertex(new  GridVertex(sidx[0] +  1, sidx[1] + 2, sidx[2]  + 0));
                    this.g.add_vertex(new  GridVertex(sidx[0] +  0, sidx[1] + 0, sidx[2]  + 1));
                    this.g.add_vertex(new  GridVertex(sidx[0] +  2, sidx[1] + 0, sidx[2]  + 1));
                    this.g.add_vertex(new  GridVertex(sidx[0] +  0, sidx[1] + 2, sidx[2]  + 1));
                    this.g.add_vertex(new  GridVertex(sidx[0] +  2, sidx[1] + 2, sidx[2]  + 1));
                    this.g.add_vertex(new  GridVertex(sidx[0] +  1, sidx[1] + 0, sidx[2]  + 2));
                    this.g.add_vertex(new  GridVertex(sidx[0] +  0, sidx[1] + 1, sidx[2]  + 2));
                    this.g.add_vertex(new  GridVertex(sidx[0] +  2, sidx[1] + 1, sidx[2]  + 2));
                    this.g.add_vertex(new  GridVertex(sidx[0] +  1, sidx[1] + 2, sidx[2]  + 2));
                }
            }
        }
    }

    //the actual marching cubes method
    public void build_isomesh(Volume v, float min_val)
    {
        float offset[3]; //realspace offset for the volume
        long dim[3]; //dim of the volume
        float fdim[3]; //dim of the volume as a float
        float dspacing[3]; //spacing between regions
        float spacing[3]; //volume spacing
        float rcord[3]; //realspace coordinate
        long gcell[3]; //grid cell in the volume
        long rdim[3]; //relative spacing of grid to graph

        long eidx = 0;

        GridVertex vox_edges[12];

        for(long d = 0; d < 3; d++)
        {
            dim[d] = v.dim[d];
            fdim[d] = (float)v.dim[d];
            offset[d] = v.offset[d];
            spacing[d] = v.spacing[d];
            dspacing[d] = fdim[d] / (float)this.gdim[0];
        }

        for(long z = 0; z < this.gdim[2]; z++)
        {
            rdim[2] = z * 2;
            for(long y = 0; y < this.gdim[1]; y++)
            {
                rdim[1] = y * 2;
                for(long x = 0; x < this.gdim[0]; x++)
                {
                    rdim[0] = x * 2;

#if 0
                    {
                        vox_edges[0x0] = new GridVertex(rdim[0] + 1, rdim[1] + 0, rdim[2] + 0);
                        vox_edges[0x1] = new GridVertex(rdim[0] + 2, rdim[1] + 1, rdim[2] + 0);
                        vox_edges[0x2] = new GridVertex(rdim[0] + 1, rdim[1] + 2, rdim[2] + 0);
                        vox_edges[0x3] = new GridVertex(rdim[0] + 0, rdim[1] + 1, rdim[2] + 0);
                        vox_edges[0x4] = new GridVertex(rdim[0] + 1, rdim[1] + 0, rdim[2] + 2);
                        vox_edges[0x5] = new GridVertex(rdim[0] + 2, rdim[1] + 1, rdim[2] + 2);
                        vox_edges[0x6] = new GridVertex(rdim[0] + 1, rdim[1] + 2, rdim[2] + 2);
                        vox_edges[0x7] = new GridVertex(rdim[0] + 0, rdim[1] + 1, rdim[2] + 2);
                        vox_edges[0x8] = new GridVertex(rdim[0] + 0, rdim[1] + 0, rdim[2] + 1);
                        vox_edges[0x9] = new GridVertex(rdim[0] + 2, rdim[1] + 0, rdim[2] + 1);
                        vox_edges[0xa] = new GridVertex(rdim[0] + 2, rdim[1] + 2, rdim[2] + 1);
                        vox_edges[0xb] = new GridVertex(rdim[0] + 0, rdim[1] + 2, rdim[2] + 1);
                    }
#endif

                    {
                        vox_edges[0x0] = new GridVertex(rdim[0] + 1, rdim[1] + 2, rdim[2] + 2);
                        vox_edges[0x1] = new GridVertex(rdim[0] + 2, rdim[1] + 2, rdim[2] + 1);
                        vox_edges[0x2] = new GridVertex(rdim[0] + 1, rdim[1] + 2, rdim[2] + 0);
                        vox_edges[0x3] = new GridVertex(rdim[0] + 0, rdim[1] + 2, rdim[2] + 1);
                        vox_edges[0x4] = new GridVertex(rdim[0] + 1, rdim[1] + 0, rdim[2] + 2);
                        vox_edges[0x5] = new GridVertex(rdim[0] + 2, rdim[1] + 0, rdim[2] + 1);
                        vox_edges[0x6] = new GridVertex(rdim[0] + 1, rdim[1] + 0, rdim[2] + 0);
                        vox_edges[0x7] = new GridVertex(rdim[0] + 0, rdim[1] + 0, rdim[2] + 1);
                        vox_edges[0x8] = new GridVertex(rdim[0] + 0, rdim[1] + 1, rdim[2] + 2);
                        vox_edges[0x9] = new GridVertex(rdim[0] + 2, rdim[1] + 1, rdim[2] + 2);
                        vox_edges[0xa] = new GridVertex(rdim[0] + 2, rdim[1] + 1, rdim[2] + 0);
                        vox_edges[0xb] = new GridVertex(rdim[0] + 0, rdim[1] + 1, rdim[2] + 0);
                    }

                    long edge_idx = 0;

                    bool vertex[8];

                    for(long i = 0; i < 8; i++)
                    {
                        rcord[0] = offset[0] + (x + ((i >> 0) & 1)) * dspacing[0];
                        rcord[1] = offset[1] + (y + ((i >> 1) & 1)) * dspacing[1];
                        rcord[2] = offset[2] + (y + ((i >> 2) & 1)) * dspacing[2];
                        gcell[0] = (long)Math.floor((rcord[0] - offset[0]) / spacing[0]);
                        gcell[1] = (long)Math.floor((rcord[1] - offset[1]) / spacing[1]);
                        gcell[2] = (long)Math.floor((rcord[2] - offset[2]) / spacing[2]);
                        if(gcell[0] < 0 || gcell[1] < 0 || gcell[2] < 0 || gcell[0] > dim[0] || gcell[1] > dim[1] || gcell[2] > dim[2])
                        {
                            vertex[i] = false;
                        }
                        else
                        {
                            if(v.data[gcell[0], gcell[1], gcell[2]] > 0.5f)
                                vertex[i] = true;
                            else
                                vertex[i] = false;
                        }
                    }

                    edge_idx |= vertex[0] ? 128 : 0;
                    edge_idx |= vertex[1] ?  64 : 0;
                    edge_idx |= vertex[2] ?   4 : 0;
                    edge_idx |= vertex[3] ?   8 : 0;
                    edge_idx |= vertex[4] ?  16 : 0;
                    edge_idx |= vertex[5] ?  32 : 0;
                    edge_idx |= vertex[6] ?   1 : 0;
                    edge_idx |= vertex[7] ?   2 : 0;

                    long edge_val = edge_table[edge_idx];

                    if(edge_val == 0) continue;

                    for(long i = 0; tri_table[edge_idx][i] != -1; i += 3)
                    {
                        this.g.add_edge_by_data(
                            vox_edges[tri_table[edge_idx][i  ]], 
                            vox_edges[tri_table[edge_idx][i+1]],
                            new GridEdge(eidx++));
                        this.g.add_edge_by_data(
                            vox_edges[tri_table[edge_idx][i+1]], 
                            vox_edges[tri_table[edge_idx][i+2]],
                            new GridEdge(eidx++));
                        this.g.add_edge_by_data(
                            vox_edges[tri_table[edge_idx][i+2]], 
                            vox_edges[tri_table[edge_idx][i  ]],
                            new GridEdge(eidx++));
                    }
                }
            }
        }

        this.fill_reals((float*)v.offset, (float*)v.spacing);
    }

    //fill in the real coordinates
    public void fill_reals(float *offset, float *spacing)
    {
        foreach(var vd in this.g.vertices.get_keys())
        {
            var v = g.vertices[vd];
            v.data.realize(offset, spacing);
        }
    }

    public void clear_graph()
    {
        this.g.clear_edges();
    }

    public void trim()
    {
        foreach(var vd in this.g.vertices.get_keys())
        {
            var v = this.g.vertices[vd];

            if(v.connected_edges.size < 1)
            {
                this.g.remove_vertex(vd);
            }
        }
    }
}
