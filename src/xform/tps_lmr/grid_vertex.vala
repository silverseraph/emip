public class GridVertex
{
    public long cord[3]; //coordinate in the graph
    public float real[3]; //realspace coordinate in the graph

    public GridVertex(long x, long y, long z)
    {
        this.cord[0] = x;
        this.cord[1] = y;
        this.cord[2] = z;
    }

    public static uint hash(GridVertex a)
    {
        return (uint)(a.cord[0]+ 29 * (a.cord[1] + (29 * a.cord[2])));
    }

    public static bool equal(GridVertex a, GridVertex b)
    {
        return (a.cord[0] == b.cord[0] && a.cord[1] == b.cord[1] && a.cord[2] ==  b.cord[2]);
    }

    public void realize(float *offset, float *spacing)
    {
        this.real[0] = offset[0] + (this.cord[0] * spacing[0]);
        this.real[1] = offset[1] + (this.cord[1] * spacing[1]);
        this.real[2] = offset[2] + (this.cord[2] * spacing[2]);
    }
}
