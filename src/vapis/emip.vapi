[PrintfFormat]
[CCode(cheader_filename="emip_utils.h", cname="log_fubar")]
public void log_fubar(string fmt, ...);

[PrintfFormat]
[CCode(cheader_filename="emip_utils.h", cname="log_err")]
public void log_err(string fmt, ...);

[PrintfFormat]
[CCode(cheader_filename="emip_utils.h", cname="log_warn")]
public void log_warn(string fmt, ...);

[PrintfFormat]
[CCode(cheader_filename="emip_utils.h", cname="log_info")]
public void log_info(string fmt, ...);

[PrintfFormat]
[CCode(cheader_filename="emip_utils.h", cname="log_infos")]
public void log_infos(string fmt, ...);

[CCode(cheader_filename="emip_utils.h", cname="debug_wait")]
public void debug_wait();

[PrintfFormat]
[CCode(cheader_filename="emip_utils.h", cname="log_nifty")]
public void log_nifty(string fmt, ...);

[CCode(cheader_filename="emip_utils.h", cname="emip_unused", simple_generics=true)]
public void unused<T>(T unused);

[CCode(cheader_filename="emip_utils.h", cname="VOL_IDX")]
public long vol_idx(long x, long y, long z, long *dim);

[CCode(cheader_filename="emip_utils.h", cname="PHI_START")]
public void phi_start();

[CCode(cname="posix_memalign")]
public int posix_memalign(void **ptr, size_t alignment, size_t size);

[CCode(cheader_filename="emip_utils.h", cname="get_extensions")]
public void get_extensions();
