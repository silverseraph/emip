public class RegGroup
{
    /* PARAMS */
    public Params parms;

    /* MEMBERS */
    public TpslmrXform best_xform;
    public TpslmrXform checkpoint_xform;
    public TpslmrXform temp_xform;

    /* VOLUMES */
    public Volume target_volume;
    public Volume original_volume;
    public Volume best_volume;
    public Volume checkpoint_volume;
    public Volume temp_volume;
    public Volume diff_volume;

    /* LANDMARKS */
    public Landmarks target_landmarks;
    public Landmarks original_landmarks;
    public Landmarks best_landmarks;
    public Landmarks checkpoint_landmarks;
    public Landmarks temp_landmarks;

    /* MODULE */
    public TpslmrXformMethod xfm;

    /* OPTIMIZER */
    public TpslmrOptimizer optimizer;

    /* ERROR */
    public float best_err;
    public float temp_err;

    public RegGroup(string[] args)
    {
        this.xfm = null;
        if(args.length < 2)
        {
            Params.show_help(this.parms, args[0]);
            exit(1);
        }
        else
        {
            switch(args[1])
            {
                case "tps_lmr":
                    this.xfm = new TpslmrXformMethod();
                    break;
                default:
                    log_err("Unrecognized or unsupported xform name: \"%s\"\n", args[1]);
                    log_err("Defaulting to \"tps_lmr\"\n");
                    this.xfm = new TpslmrXformMethod();
                    break;
            }
        }

        this.parms = new Params(args, this.xfm);
        this.best_err = float.MAX;
        this.temp_err = float.MAX;

        this.print_validate(args);

        if(this.xfm.requires_landmarks() && (this.parms.landmarks_a == null || this.parms.landmarks_b == null))
        {
            log_fubar("The selected xform requires landmarks, but landmarks for both input volumes have not been provided.  Exiting...\n");
        }

        //create volumes
        {
            this.target_volume = new Volume.from_mha(this.parms.static_fn);
            this.original_volume = new Volume.from_mha(this.parms.moving_fn);
            this.best_volume = this.original_volume.meta_clone();
            this.checkpoint_volume = this.original_volume.meta_clone();
            this.temp_volume = this.original_volume.full_clone();
            this.diff_volume = this.temp_volume.meta_clone();
            Volume.diff(this.diff_volume, this.target_volume, this.temp_volume);
        }

        //create landmarks
        {
            try
            {
                if(this.parms.landmarks_a != null) 
                {
                    this.target_landmarks = new Landmarks.from_fcsv(this.parms.landmarks_a, this.target_volume.spacing, this.target_volume.offset);
                }
                else 
                {
                    this.target_landmarks = new Landmarks("dynamic_target_landmarks");
                }
                if(this.parms.landmarks_b != null) 
                {
                    this.original_landmarks = new Landmarks.from_fcsv(this.parms.landmarks_b, this.original_volume.spacing, this.original_volume.offset);
                }
                else 
                {
                    this.original_landmarks = new Landmarks("dynamic_original_landmarks");
                }

                this.best_landmarks = this.original_landmarks.clone();
                this.checkpoint_landmarks = this.original_landmarks.clone();
                this.temp_landmarks = this.original_landmarks.clone();
            }
            catch(Error e)
            {
                log_fubar("%s\n", e.message);
            }
        }

        this.xfm.configure(this);
    }

    private void print_validate(string[] args)
    {
        if(!parms.valid)
        {
            if(parms.static_fn == null)
            {
                stderr.printf("Static image filename unset.\n");
            }
            if(parms.moving_fn == null) 
            {
                stderr.printf("Moving image filename unset.\n");
            }
            if(parms.output_fn == null)
            {
                stderr.printf("Output image filename unset.\n");
            }
            if(parms.outvec_fn == null)
            {
                stderr.printf("Output vectors filename unset.\n");
            }
            if(parms.tolerance <= 0)
            {
                stderr.printf("Invalid tolerance value.  The tolerance value must be greater than zero.\n");
            }
            if(parms.max_iters < 0)
            {
                stderr.printf("Maximum iterations must be greater than zero.\n");
            }
            if((parms.landmarks_a == null) && (parms.landmarks_b != null))
            {
                stderr.printf("Landmark files must be provided for both of the volumes, or neither of the volumes.  Currently there is a file for \"b\", but not one for \"a\".\n");
            }
            if((parms.landmarks_a != null) && (parms.landmarks_b == null))
            {
                stderr.printf("Landmark files must be provided for both of the volumes, or neither of the volumes.  Currently there is a file for \"a\", but not one for \"b\".\n");
            }

            stdout.putc('\n');
            Params.show_help(this.parms, args[0]);
            exit(1);
        }
    }

    public int run()
    {
        if((this.best_err = this.optimizer.score(ScoreType.INITIAL)) < parms.tolerance)
        {
            stdout.printf("Volumes match sufficiently (%f | %f), will not perform the xform.\n", this.best_err, parms.tolerance);
            this.original_volume.write(this.parms.output_fn);
            return 0;
        }
        else
        {
            stdout.printf("Score:\n\tbest: %f\n", this.best_err);
        }

        int iter = 0;

#if DEBUG
        if(this.target_landmarks != null)
        {
            stdout.printf("\ntarget_landmarks\n");
            this.target_landmarks.print();
        }
        if(this.original_landmarks != null)
        {
            stdout.printf("\noriginal landmarks:\n");
            this.original_landmarks.print();
        }
#endif

        /*
        * Warp, score, save value, improve
        */
        while(iter < this.parms.max_iters)
        {
            stdout.printf("Iteration: %d\n", iter);

            this.temp_xform.warp();

            this.temp_err = this.optimizer.score(ScoreType.NORMAL);

            stdout.printf("Score[%3d]:\n\tbest: %f\n\titer: %f\n", iter, this.best_err, this.temp_err);

#if DEBUG
            this.temp_volume.write("iter%d-temp.mha".printf(iter));
#endif

            //save off the xform, volume, and landmarks
            if(this.temp_err < this.best_err)
            {
                this.best_err = this.temp_err;
                this.best_xform  = this.temp_xform;
                this.best_volume.copy_data(this.temp_volume);
                this.best_landmarks = temp_landmarks.clone();
            }

            this.optimizer.improve();

            iter++;
        }

        if(this.best_err < this.parms.tolerance)
        {
            stdout.printf("Transform successful.\n");
        }
        else
        {
            stdout.printf("Transform failed.\n");
        }

        this.best_volume.write(this.parms.output_fn);
        this.best_xform.write(this.parms.outvec_fn);

#if DEBUG
        this.temp_xform.write(this.parms.outvec_fn + "-debug");
#endif

        return 0;
    }

    public void finish()
    {
    }
}
