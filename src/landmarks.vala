[CCode(cname="Landmarks")]
public class Landmarks
{
    /* CONSTANTS */
    public const string head_rex_str = """^\s*#\s*([^=\s]+)\s*=(.*)$""";
    public const string data_rex_str = """^\s*([^,]+)\s*,\s*([+-]?[0-9]*\.?[0-9]+)\s*,\s*([+-]?[0-9]*\.?[0-9]+)\s*,\s*([+-]?[0-9]*\.?[0-9]+)\s*,\s*([01])\s*,\s*([01])\s*$""";

    /* MEMBERS */
    //public HashTable<string, LandmarkPoint> landmarks;
    public Vala.HashMap<string, LandmarkPoint> landmarks;
    public string name;
    //public int num_points;
    public int num_points
    {
        get
        {
            return this.landmarks.size;
        }
    }
    public bool valid;
    public string type;

    public Landmarks(string name = "")
    {
        //doesn't do anything for now
        this.name = name;
        this.type = "fcsv";
        this.landmarks = new Vala.HashMap<string, LandmarkPoint>(str_hash, str_equal);
        this.valid = true;
    }

    public Landmarks.from_fcsv(string file_name, float[] spacing, float[] origin) throws FileError
    {
        //configure member variables
        this.valid = true;
        this.name = "";
        this.type = "fcsv";
        this.landmarks = new Vala.HashMap<string, LandmarkPoint>(str_hash, str_equal);

        //local hashtable for header lines
        Vala.HashMap<string, string> headers = new Vala.HashMap<string, string>(str_hash, str_equal);
        var ios = FileStream.open(file_name, "r");

        if(ios == null)
        {
            this.valid = false;
            return;
        }

        string line = null;
        bool read_header = true; //set to false after encountering the columns header

        ios.read_line(); //throw out the first line saying that it's an FCSV

        Regex head_rex = null;
        Regex data_rex = null;

        try
        {
            head_rex = new Regex(head_rex_str);
            data_rex = new Regex(data_rex_str);
        }
        catch(Error e)
        {
            stdout.printf("Error: %s\n", e.message);
            critical("Encountered an error creating regular expressions for head and data lines.\n");
            return;
        }

        assert(head_rex != null && data_rex != null);

        MatchInfo match_info;

        while((line = ios.read_line()) != null)
        {
            if(read_header)
            {
                if(head_rex.match(line, 0, out match_info))
                {
                    log_info("Header found: %s = %s\n", match_info.fetch(1), match_info.fetch(2));
                    headers[match_info.fetch(1)] = match_info.fetch(2);

                    if(match_info.fetch(1) == "name") this.name = match_info.fetch(2);
                    if(match_info.fetch(1) == "columns") read_header = false;
                }
                else
                {
                    log_warn("Line: \"%s\" does not match the header format, \"%s\".\n", line, head_rex.get_pattern());
                }
            }
            else
            {
                if(data_rex.match(line, 0, out match_info))
                {
                    log_info("point: [%s, %s, %s, %s, %s, %s]\n", match_info.fetch(1), match_info.fetch(2), match_info.fetch(3), match_info.fetch(4), match_info.fetch(5), match_info.fetch(6));
                    this.add_point(
                        match_info.fetch(1), 
                        -(float)double.parse(match_info.fetch(2)), 
                        -(float)double.parse(match_info.fetch(3)), 
                        (float)double.parse(match_info.fetch(4)), 
                        1f, 
                        (match_info.fetch(5) == "1") ? true : false, 
                        (match_info.fetch(6) == "1") ? true : false
                    );
                }
                else
                {
                    log_warn("Line: \"%s\" does not match the data format, '%s'.\n", line, data_rex.get_pattern());
                }
            }
        }

        debug_wait();
    }

    public Landmarks.from_rcsv(string file_name) throws FileError
    {
        //configure member variables
        this.valid = true;
        this.name = "";
        this.type = "rcsv";
        this.landmarks = new Vala.HashMap<string, LandmarkPoint>(str_hash, str_equal);

        //local hashtable for header lines
        Vala.HashMap<string, string> headers = new Vala.HashMap<string, string>(str_hash, str_equal);
        var ios = FileStream.open(file_name, "r");

        if(ios == null)
        {
            this.valid = false;
            return;
        }

        string line = null;
        bool read_header = true; //set to false after encountering the columns header

        ios.read_line(); //throw out the first line saying that it's an FCSV

        Regex head_rex = null;
        Regex data_rex = null;

        try
        {
            head_rex = new Regex(head_rex_str);
            data_rex = new Regex(data_rex_str);
        }
        catch(Error e)
        {
            stdout.printf("Error: %s\n", e.message);
            critical("Encountered an error creating regular expressions for head and data lines.\n");
            return;
        }

        assert(head_rex != null && data_rex != null);

        MatchInfo match_info;

        while((line = ios.read_line()) != null)
        {
            if(read_header)
            {
                if(head_rex.match(line, 0, out match_info))
                {
#if DEBUG
                    stdout.printf("Header found: %s = %s\n", match_info.fetch(1), match_info.fetch(2));
#endif
                    headers[match_info.fetch(1)] = match_info.fetch(2);

                    if(match_info.fetch(1) == "name") this.name = match_info.fetch(2);
                    if(match_info.fetch(1) == "columns") read_header = false;
                }
                else
                {
                    stderr.printf("Line: \"%s\" does not match the header format, '%s'.\n", line, head_rex.get_pattern());
                }
            }
            else
            {
                if(data_rex.match(line, 0, out match_info))
                {
                    this.add_point(
                        match_info.fetch(1), 
                        (float)double.parse(match_info.fetch(2)), 
                        (float)double.parse(match_info.fetch(3)), 
                        (float)double.parse(match_info.fetch(4)), 
                        1f, 
                        (match_info.fetch(5) == "1") ? true : false, 
                        (match_info.fetch(6) == "1") ? true : false
                    );
                }
                else
                {
                    stderr.printf("Line: \"%s\" does not match the data format, '%s'.\n", line, data_rex.get_pattern());
                }
            }
        }
    }

    public Landmarks clone()
    {
        var ret = new Landmarks();

        ret.name = this.name;
        ret.valid = this.valid;

        foreach(var pn in this.landmarks.get_keys())
        {
            ret.landmarks[pn] = this.landmarks[pn].clone();
        }

        return ret;
    }

    public void print(FileStream? fs = null)
    {
        if(fs == null) fs = stdout;
        fs.printf("Landmark set:\n");
        fs.printf("\tname: %s\n", this.name);
        fs.printf("\tnum_points: %d\n", this.num_points);
        fs.printf("\tpoints: (name, x, y, z, selected, visible)\n");
        foreach(var pn in this.landmarks.get_keys())
        {
            var point = this.landmarks[pn];
            fs.printf("\t%s\n\t\t%f\n\t\t%f\n\t\t%f\n\t\t%s\n\t\t%s\n", point.name, point.x, point.y, point.z, point.selected.to_string(), point.visible.to_string());
        }
    }

    public bool add_point(string name, float x, float y, float z, float weight = 1f, bool selected = true, bool visible = true)
    {
        foreach(var point in this.landmarks.get_values())
        {
            if(point.x == x && point.y == y && point.z == z) return false;
        }

        this.landmarks[name] = new LandmarkPoint(name, x, y, z, weight, selected, visible);

        return true;
    }

    public void rem_point(string name)
    {
        this.landmarks.remove(name);
    }

    public class LandmarkPoint
    {
        public string name;
        public float x;
        public float y;
        public float z;
        public float weight;
        public bool selected;
        public bool visible;

        public LandmarkPoint(string name, float x, float y, float z, float weight, bool selected, bool visible)
        {
            this.name = name;
            this.x = x;
            this.y = y;
            this.z = z;
            this.weight = weight;
            this.selected = selected;
            this.visible  = visible;
        }

        public LandmarkPoint clone()
        {
            return new LandmarkPoint(this.name, this.x, this.y, this.z, this.weight, this.selected, this.visible);
        }

        public string to_string()
        {
            return "\t%s\n\t\t%f\n\t\t%f\n\t\t%f\n\t\t%s\n\t\t%s\n".printf(this.name, this.x, this.y, this.z, this.selected.to_string(), this.visible.to_string());
        }
    }
}
