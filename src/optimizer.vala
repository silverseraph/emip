public interface Optimizer : Object
{
    /**
     * Determines the score between two volumes, using all available parameters.  Implementation of the score is left up to the creator of the module.
     *
     * @return The overall score of the comparison
     */
    public abstract float score(ScoreType type);

    /**
     * Attempts to improve upon the previous transformation 'xf' based on the region scores.
     *
     */
    public abstract void improve();
}
