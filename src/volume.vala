public class Volume
{
    [CCode(cheader_filename="volume_funcs.h", has_target="false", cname="volume_internal_diff")]
    private static extern void volume_internal_diff(int64 len, float *da, float *db, float *dr);

#if USE_PHI
    [CCode(cheader_filename="volume_funcs.h", has_target="false", cname="volume_phi_localize")]
    private static extern void volume_phi_localize(float *data, long size);

    [CCode(cheader_filename="volume_funcs.h", has_target="false", cname="volume_phi_delocalize")]
    private static extern void volume_phi_delocalize(float *data, long size);

    public bool phi_local = false;

#else
    public bool phi_local = true;
#endif

    public float[,,] data;

    public long dim[3];
    public float offset[3];
    public float spacing[3];

    public float step[9];
    public float proj[9];
    public float d[3]; //width in reals
    public float min[3]; //min value in reals
    public float max[3]; //max value in reals

    public long n_pix;
    public ulong pix_size;
    public float max_d;

    public PixType pix_type;

    public float dcos[9];
    public bool has_dcos = false;

    public float min_pix;
    public float max_pix;

    ~Volume()
    {
#if USE_PHI
        this.phi_free();
#endif
    }

    public Volume(long x, long y, long z)
    {
        this.max_d = 1f;
        for(int d = 0; d < 3; d++)
        {
            this.spacing[d] = 0f;
            this.offset[d] = 0f;
            this.d[d] = 1f;
            this.min[d] = 0f;
            this.max[d] = 1f;
        }

        this.dim[0] = x;
        this.dim[1] = y;
        this.dim[2] = z;

#if USE_PHI
        this.phi_local = false;
#endif

        for(int i = 0; i < this.dcos.length; i++) this.dcos[i] = 0;
        this.dcos[0] = 1;
        this.dcos[4] = 1;
        this.dcos[8] = 1;

        this.n_pix = this.dim[0] * this.dim[1] * this.dim[2];

        this.min_pix = 0;
        this.max_pix = 1;

        this.data = new float[this.dim[0], this.dim[1], this.dim[2]];
        //posix_memalign((void**)(&this.data), 64, sizeof(float) * this.dim[0] * this.dim[1] * this.dim[2]);
    }

    public Volume.from_mha(string file_name)
    {
#if USE_PHI
        this.phi_local = false;
#endif
        //set default dcos values
        for(int i = 0; i < this.dcos.length; i++) this.dcos[i] = 0;
        this.dcos[0] = 1;
        this.dcos[4] = 1;
        this.dcos[8] = 1;

        this.pix_type = PixType.UNDEFINED;
        this.pix_size = 0;

        int tmp = 0;
        int a = 0, b = 0, c = 0;

        float[9] dc = {1, 0, 0, 0, 1, 0, 0, 0, 1};
        bool have_dcos = false;

        bool big_endian_input = false;

        this.dim = {0, 0, 0};

        //set defaults
        this.data = new float[this.dim[0], this.dim[1], this.dim[2]];
        //posix_memalign((void**)(&this.data), 64, sizeof(float) * this.dim[0] * this.dim[1] * this.dim[2]);
        //now read the actual data
        FileStream ios = FileStream.open(file_name, "rb");

        if(ios == null)
        {
            log_fubar("Cannot locate file: %s\n", file_name);
        }

        string line;

        //while there is a line to read
        while((line = ios.read_line()) != null)
        {
            line = line.strip(); //trim in place
            if(line == null) continue;

            log_info("Line = \"%s\"\n", line);

            if("ElementDataFile = LOCAL" == line) break;
            if(line.scanf("DimSize = %d %d %d", &a, &b, &c) == 3) 
            {
                this.dim[0] = a;
                this.dim[1] = b;
                this.dim[2] = c;
                this.n_pix = this.dim[0] * this.dim[1] * this.dim[2];
                continue;
            }
            if(line.scanf("Offset = %g %g %g", &this.offset[0], &this.offset[1], &this.offset[2]) == 3) continue;
            if(line.scanf("ElementSpacing = %g %g %g", &this.spacing[0], &this.spacing[1], &this.spacing[2]) == 3) continue;
            if(line.scanf("TransformMatrix = %g %g %g %g %g %g %g %g %g", &dc[0], &dc[3], &dc[6], &dc[1], &dc[4], &dc[7], &dc[2], &dc[5], &dc[8]) == 9)
            {
                have_dcos = true;
                continue;
            }
            if(line.scanf("ElementNumberOfChannels = %d", &tmp) == 1) 
            {
                log_info("Using Interleave element type\n");
                if(this.pix_type == PixType.UNDEFINED || this.pix_type == PixType.FLOAT) 
                {
                    this.pix_type = PixType.VF_FLOAT_INTERLEAVED;
                    this.pix_size = 3 * sizeof(float);
                    log_fubar("Refusing to handle VF_FLOAT_INTERLEAVED.  Exiting...\n");
                }
                continue;
            }
            if(line == "ElementType = MET_FLOAT")
            {
                log_info("Using ElementType = MET_FLOAT\n");
                if(this.pix_type == PixType.UNDEFINED)
                {
                    this.pix_type = PixType.FLOAT;
                    this.pix_size = sizeof(float);
                }
                continue;
            }
            if(line == "ElementType = MET_SHORT")
            {
                log_info("Using ElementType = MET_SHORT\n");
                if(this.pix_type == PixType.UNDEFINED)
                {
                    this.pix_type = PixType.SHORT;
                    this.pix_size = sizeof(short);
                }
                continue;
            }
            if(line == "ElementType = MET_UCHAR")
            {
                log_info("Using ElementType = MET_UCHAR\n");
                this.pix_type = PixType.UCHAR;
                this.pix_size = sizeof(uint8);
                log_fubar("Refusing to handle UCHAR.  Exiting...\n");
                continue;
            }
            if(line == "BinaryDataByteOrderMSB = TRUE")
            {
                big_endian_input = true;
            }
        }


        //cache realspace values for the volume computations later
        this.max_d = float.MIN;
        for(long d = 0; d < 3; d++)
        {
            this.min[d] = this.offset[d];
            this.d[d] = this.dim[d] * this.spacing[d];
            this.max[d] = this.min[d] + this.d[d];
            if(this.d[d] < this.max_d) this.max_d = this.d[d];
        }

        if(this.has_dcos)
        {
            for(int i = 0; i < 9; i++)
                this.dcos[i] = dc[i];
        }

        if(this.pix_size < 1)
        {
            log_fubar("Unable to interpret this mha data.  Exiting...\n");
        }

        this.data = new float[this.dim[0], this.dim[1], this.dim[2]];
        size_t bytes_read = 0;

        uint8[] d_buff = new uint8[this.n_pix * this.pix_size];
        if((bytes_read = ios.read(d_buff, this.pix_size)) != this.n_pix)
        {
            log_fubar("Unexpected early end of volume data.\n\tExpected: %lu elements, but received: %lu\n\tExiting...\n", this.n_pix, (ulong)bytes_read);
        }

        if(big_endian_input ^ big_endian())
        {
            stdout.printf("DOING BIG <-> LITTLE swap.  Current format is %s\n", big_endian() ? "BIG" : "LITTLE");
            if(this.pix_size == 4) endian4_swap((uint8*)d_buff, d_buff.length);
            else if(this.pix_size == 2) endian2_swap((uint8*)d_buff, d_buff.length);
        }

        const float uint8maxf = (float)uint8.MAX;
        short s_temp = 0;
        float f_temp = 0;
        ulong idx = 0;
        ulong stride = 0;
#if DEBUG
        short s_min_value = short.MAX;
        short s_max_value = short.MIN;
#endif

        switch(this.pix_type)
        {
            // IEEE float
            case PixType.FLOAT:
                const float float_min = 1407;
                const float float_range = 4407;
                stride = (ulong)sizeof(float);
                for(int z = 0; z < this.dim[2]; z++)
                {
                    for(int y = 0; y < this.dim[1]; y++)
                    {
                        for(int x = 0; x < this.dim[0]; x++)
                        {
                            Memory.copy(&f_temp, &(d_buff[idx]), sizeof(float));
                            this.data[x,y,z] = (f_temp + float_min) / float_range;
                            idx += stride;
                        }
                    }
                }
                break;
                //unsigned 8 bit int
            case PixType.UCHAR:
                stride = (ulong)sizeof(uint8);
                for(int z = 0; z < this.dim[2]; z++)
                {
                    for(int y = 0; y < this.dim[1]; y++)
                    {
                        for(int x = 0; x < this.dim[0]; x++)
                        {
                            this.data[x,y,z] = ((float)d_buff[idx])/uint8maxf;
                            idx += stride;
                        }
                    }
                }
                break;
                //signed 16bit int
            case PixType.SHORT:
                const float short_range = 4407;
                stride = (ulong)sizeof(short);
                for(int z = 0; z < this.dim[2]; z++)
                {
                    for(int y = 0; y < this.dim[1]; y++)
                    {
                        for(int x = 0; x < this.dim[0]; x++)
                        {
                            s_temp = (short)(d_buff[idx+0] << 0) | (short)(d_buff[idx+1] << 8);

                            //this.data[x,y,z] = (float)(s_buff[idx / stride]);
                            this.data[x,y,z] = ((float)(s_temp + 1407)) / short_range;
                            idx += stride;
                        }
                    }
                }
                break;
                //something else, IDFC for now
            default:
                log_fubar("Unhandled PixType, \"%s\", Exiting...\n", this.pix_type.to_string());
                break;
        }

        this.min_pix = float.MAX;
        this.max_pix = float.MIN;
        for(long x = 0; x < this.dim[0]; x++)
        {
            for(long y = 0; y < this.dim[1]; y++)
            {
                for(long z = 0; z < this.dim[2]; z++)
                {
                    this.min_pix = float.min(this.min_pix, this.data[x,y,z]);
                    this.max_pix = float.max(this.max_pix, this.data[x,y,z]);
                }
            }
        }
        if(this.min_pix >= this.max_pix)
        {
            for(long x = 0; x < this.dim[0]; x++)
            {
                for(long y = 0; y < this.dim[1]; y++)
                {
                    for(long z = 0; z < this.dim[2]; z++)
                    {
                        this.data[x,y,z] = 0.5f;
                    }
                }
            }
        }
        else
        {
            float delta = this.max_pix - this.min_pix;
            for(long x = 0; x < this.dim[0]; x++)
            {
                for(long y = 0; y < this.dim[1]; y++)
                {
                    for(long z = 0; z < this.dim[2]; z++)
                    {
                        this.data[x,y,z] = (this.data[x,y,z] - this.min_pix) / delta;
                    }
                }
            }
        }

        ios = null;

        log_info("Created volume:\n\treal min: %f %f %f\n\treal max: %f %f %f\n\tread delta: %f %f %f\n", this.min[0], this.min[1], this.min[2], this.max[0], this.max[1], this.max[2], this.d[0], this.d[1], this.d[2]);
    }

    public Volume meta_clone()
    {
        Volume ret = new Volume(this.dim[0], this.dim[1], this.dim[2]);
#if USE_PHI
        ret.phi_local = false;
#endif
        ret.n_pix = this.n_pix;
        ret.pix_size = this.pix_size;
        ret.has_dcos = this.has_dcos;
        ret.pix_type = this.pix_type;
        ret.max_d = this.max_d; //max dimension for TPS/TVS
        ret.min_pix = this.min_pix;
        ret.max_pix = this.max_pix;
        for(int d = 0; d < 3; d++)
        {
            //ret.dim[d] = this.dim[d];
            ret.offset[d] = this.offset[d];
            ret.spacing[d] = this.spacing[d];
            ret.d[d] = this.d[d];
            ret.min[d] = this.min[d];
            ret.max[d] = this.max[d];
        }
        for(int i = 0; i < 9; i++)
        {
            ret.step[i] = this.step[i];
            ret.proj[i] = this.proj[i];
            ret.dcos[i] = this.dcos[i];
        }
        return ret;
    }

    public Volume full_clone()
    {
        Volume ret = this.meta_clone();
#if USE_PHI
        ret.phi_local = false;
#endif
        for(long x = 0; x < ret.dim[0]; x++)
        {
            for(long y = 0; y < ret.dim[1]; y++)
            {
                for(long z = 0; z < ret.dim[2]; z++)
                {
                    ret.data[x,y,z] = this.data[x,y,z];
                }
            }
        }
        return ret;
    }

    public static void diff(Volume dst, Volume a, Volume b)
    {
        assert(a.dim[0] == b.dim[0] && a.dim[1] == b.dim[1] && a.dim[2] == b.dim[2]);
        assert(a.dim[0] == dst.dim[0] && a.dim[1] == dst.dim[1] && a.dim[2] == dst.dim[2]);

        long len = a.n_pix;

        //create simple pointers for OMP/PHI/CUDA
        float *da = (float*)a.data;
        float *db = (float*)b.data;
        float *dr = (float*)dst.data;

        a.phi_alloc();
        b.phi_alloc();

        volume_internal_diff(len, da, db, dr);
    }

    public void get_real_coord(long x, long y, long z, out float fx, out float fy, out float fz)
    {
        fx = (x * this.spacing[0]) + this.offset[0];
        fy = (y * this.spacing[1]) + this.offset[1];
        fz = (z * this.spacing[2]) + this.offset[2];
    }

    public void write(string file_name)
    {
        FileStream ios = FileStream.open(file_name, "wb");

        if(ios == null)
        {
            log_fubar("Failed to write to %s\n", file_name);
        }

        string element_type = "MET_FLOAT";
        switch(this.pix_type)
        {
            case PixType.UCHAR:
                element_type = "MET_UCHAR";
                break;
            case PixType.SHORT:
                element_type = "MET_SHORT";
                break;
            case PixType.FLOAT:
                element_type = "MET_FLOAT";
                break;
            default:
                log_fubar("Unhandled type: \"%s\".  Exiting...\n", this.pix_type.to_string());
                return;
        }

        ios.printf("ObjectType = Image\nNDims = 3\nBinaryData = True\nBinaryDataByteOrderMSB = False\nTransformMatrix = %g %g %g %g %g %g %g %g %g\nOffset = %g %g %g\nCenterOfRotation = 0 0 0\nElementSpacing = %g %g %g\nDimSize = %lld %lld %lld\nAnatomicalOrientation = RAI\n%sElementType = %s\nElementDataFile = LOCAL\n", 
                this.dcos[0], this.dcos[1], this.dcos[2], this.dcos[3], this.dcos[4], this.dcos[5], this.dcos[6], this.dcos[7], this.dcos[8], 
                this.offset[0], this.offset[1], this.offset[2],
                this.spacing[0], this.spacing[1], this.spacing[2],
                this.dim[0], this.dim[1], this.dim[2],
                ((this.pix_type == PixType.VF_FLOAT_INTERLEAVED) ? "ElementNumberOfChannels = 3\n" : ""),
                element_type);

        ios.flush();

        if(this.min_pix >= this.max_pix) //invalid case
        {
            for(long x = 0; x < this.dim[0]; x++)
            {
                for(long y = 0; y < this.dim[1]; y++)
                {
                    for(long z = 0; z < this.dim[2]; z++)
                    {
                        this.data[x,y,z] = 0.5f;
                    }
                }
            }
        }
        else //valid case
        {
            float delta = this.max_pix - this.min_pix;
            for(long x = 0; x < this.dim[0]; x++)
            {
                for(long y = 0; y < this.dim[1]; y++)
                {
                    for(long z = 0; z < this.dim[2]; z++)
                    {
                        this.data[x,y,z] = (this.data[x,y,z] * delta) + this.min_pix;
                    }
                }
            }
        }

        uint8[] converted_data = convert_data();

        write_volume_data(converted_data, ios);

        ios = null;
    }

    //convert data from vala [x,y,z] to mha [z,y,x] and size
    private uint8[] convert_data()
    {
        int x, y, z;
        ulong odo = 0; //One D Offset
        uint8[] ret;
        uint8 uchar_val;
        short short_val;
        float float_val;

        //uint8 buff[4] = {0, 0, 0, 0}; //copy data to this before moving to uint8 ret array
        switch(this.pix_type)
        {
            //8 bit unsigned int
            case PixType.UCHAR:
                ret = new uint8[this.n_pix * sizeof(uint8)];
                for(z = 0; z < this.dim[2]; z++)
                {
                    for(y = 0; y < this.dim[1]; y++)
                    {
                        for(x = 0; x < this.dim[0]; x++)
                        {
                            //convert to uint8
                            uchar_val = (uint8)(this.data[x,y,z] * uint8.MAX);
                            Memory.copy(&(ret[odo]), &uchar_val, sizeof(uint8));
                            odo += sizeof(uint8);
                        }
                    }
                }
                break;
                //16 bit signed int
            case PixType.SHORT:
                ret = new uint8[this.n_pix * sizeof(short)];
                const float short_range = 4407;
                const float short_min = -1407;
                for(z = 0; z < this.dim[2]; z++)
                {
                    for(y = 0; y < this.dim[1]; y++)
                    {
                        for(x = 0; x < this.dim[0]; x++)
                        {
                            short_val = (short)((short_range * this.data[x,y,z]) + short_min);

                            Memory.copy(&(ret[odo]), &short_val, sizeof(short));
                            odo += sizeof(short);
                        }
                    }
                }
                break;
            case PixType.FLOAT:
                ret = new uint8[this.n_pix * sizeof(float)];
                for(z = 0; z < this.dim[2]; z++)
                {
                    for(y = 0; y < this.dim[1]; y++)
                    {
                        for(x = 0; x < this.dim[0]; x++)
                        {
                            float_val = (this.data[x,y,z] * 4407f) - 1407f;
                            Memory.copy(&(ret[odo]), &float_val, sizeof(float));
                            odo += sizeof(float);
                        }
                    }
                }
                break;
            default:
                log_fubar("Unhandled type.  Cannot convert to: \"%s\".  Exiting...\n", this.pix_type.to_string());
                return new uint8[4]; //unreachable
                //break;
        }

        return ret;
    }

    private void write_volume_data(uint8[] data, FileStream ios)
    {
        //swap if needed, only if the system is big endian
        if(big_endian())
        {
            if(this.pix_size == 2)
            {
                endian2_swap((void*)data, data.length);
            }
            else if(this.pix_size == 4)
            {
                endian4_swap((void*)data, data.length);
            }
            else
            {
                log_fubar("Unhandled pixel size: %lu.  Exiting...\n", this.pix_size);
            }
        }

        log_info("Size of binary to write: %u\n", data.length);

        ios.write(data);
    }

    public void copy_data(Volume src)
    {
        assert( src.dim[0] == this.dim[0] &&
                src.dim[1] == this.dim[1] &&
                src.dim[2] == this.dim[2]);

        Memory.copy((void*)this.data, (void*)src.data, sizeof(float) * src.dim[0] * src.dim[1] * src.dim[2]);
    }

    public void phi_alloc()
    {
#if USE_PHI
        if(!this.phi_local)
        {
            this.phi_local = true;
            volume_phi_localize((float*)this.data, this.dim[0] * this.dim[1] * this.dim[2]);
        }
#endif
    }

    public void phi_free()
    {
#if USE_PHI
        if(this.phi_local)
        {
            this.phi_local = false;
            volume_phi_delocalize((float*)this.data, this.dim[0] * this.dim[1] * this.dim[2]);
        }
#endif
    }
}
