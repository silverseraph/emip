#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <omp.h>
#include <glib.h>

#include "emip_utils.h"
#include "volume_funcs.h"

void volume_internal_diff(guint64 len, float *da, float *db, float *dr)
{
    guint64 i;
    log_info("Running diff <" PHI_STR " " OMP_STR ">\n");
#ifdef USE_PHI
    #pragma offload target(mic) \
        in(da,db:length(0) REUSE) \
        out(dr:length(len))
#endif
    {
#ifdef USE_OMP
        #pragma omp parallel for \
            default(none) \
            private(i) \
            shared(len, da, db, dr)
#endif
        for(i = 0; i < len; i++)
        {
            dr[i] = fabs(da[i] - db[i]);
        }
    }
}

void volume_phi_localize(float *data, glong size)
{
#ifdef USE_PHI
    #pragma offload_transfer target(mic) nocopy(data : length(size) ALLOC)
    #pragma offload_transfer target(mic) in(data : length(size) REUSE)
#endif
}

void volume_phi_delocalize(float *data, glong size)
{
#ifdef USE_PHI
    #pragma offload_transfer target(mic) out(data : length(size) FREE)
#endif
}
