public enum EdgeDirection
{
    A_TO_B = 1,
    B_TO_A = 2,
    BIDIRECTIONAL = 3;

    public static string to_string(EdgeDirection e)
    {
        switch(e)
        {
            case EdgeDirection.A_TO_B:
                return "A_TO_B";
            case EdgeDirection.B_TO_A:
                return "B_TO_A";
            case EdgeDirection.BIDIRECTIONAL:
                return "BIDIRECTIONAL";
            default:
                return "<UNKNOWN>";
        }
    }
}
