public enum ScoreType
{
    INITIAL = 0,
    LANDMARKS = 1,
    DIFF = 2,
    NORMAL = 3
}
