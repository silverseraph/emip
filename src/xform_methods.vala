/*
 * Method of loading and using modules is a modified version of what is available at www.valadoc.org/#!api=gmodule-2.0/GLib.Module
 * Author: Andrew Benton
 * Date: 2014-01-07
 */

public interface XformMethod : Object
{
    public abstract string get_name();
    public abstract bool requires_landmarks();
    public abstract void configure(RegGroup group);
    public abstract bool handle_arg(string[] args, ref int i);
    public abstract string method_help();
}
