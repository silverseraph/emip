#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>

#include <glib.h>

#include "emip_utils.h"

void get_extensions()
{
    GString *str = g_string_new(NULL);
    char *output = NULL;

#ifdef FOUND_OMP
    g_string_append(str, "OpenMP ");
#endif

#ifdef FOUND_SSE
    g_string_append(str, "SSE ");
#endif

#ifdef FOUND_SSE2
    g_string_append(str, "SSE2 ");
#endif

#ifdef FOUND_SSE3
    g_string_append(str, "SSE3 ");
#endif

#ifdef FOUND_SSE4_1
    g_string_append(str, "SSE4_1 ");
#endif

#ifdef FOUND_SSE4_2
    g_string_append(str, "SSE4_2 ");
#endif

#ifdef FOUND_AVX
    g_string_append(str, "AVX ");
#endif

#ifdef FOUND_AVX2
    g_string_append(str, "AVX2 ");
#endif

#ifdef FOUND_AVX512
    g_string_append(str, "AVX512 ");
#endif

#ifdef __cilk
    g_string_append(str, "CilkPlus ");
#endif

#ifdef __INTEL_COMPILER
    g_string_append(str, "ICC");
#endif

#ifdef __MIC__
    g_string_append(str, "MIC");
#endif

    output = g_string_free(str, false);
    printf("Available extensions: %s\n", output);
    free(output);
}
