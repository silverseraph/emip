int main(string[] args)
{
#if USE_PHI
    phi_start();
#endif

    Process.signal(ProcessSignal.FPE, sig_fpe_handler);

    var reg_group = new RegGroup(args);

    var res = reg_group.run();

    reg_group.finish();

    return res;
}

void sig_fpe_handler(int sig_num)
{
    log_err("Detected floating point error. Triggering core dump.");

    Process.raise(ProcessSignal.SEGV);
}
