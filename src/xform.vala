public abstract class Xform
{
    public abstract InterpType interp { get; set; }
    //public abstract Volume xform(Volume input);
    public abstract Xform  clone();
    public abstract void   print();
    public abstract void   warp();
    public abstract void   write(string output_fn);
}
