public class Params
{
    public string static_fn = null;
    public string moving_fn = null;
    public int max_iters;
    public float tolerance;
    public string output_fn = null;
    public string outvec_fn = null;
    public string xform = null;
    public string landmarks_a = null;
    public string landmarks_b = null;
    public bool gui;
    public XformMethod xfm;

    public bool valid
    {
        get
        {
            if(this.static_fn == null) return false;
            if(this.moving_fn == null) return false;
            if(this.output_fn == null) return false;
            if(this.outvec_fn == null) return false;
            if(this.tolerance <= 0) return false;
            if(this.max_iters <= 0) return false;
            if((this.landmarks_a == null && this.landmarks_b != null) || (this.landmarks_a != null && this.landmarks_b == null)) return false;
            return true;
        }
    }

    public Params(string[] args, XformMethod xfm)
    {
        this.tolerance = 0.0001f;
        this.max_iters = 100; //default
        this.xform = xfm.get_name();
        this.landmarks_a = null;
        this.landmarks_b = null;
        this.xfm = xfm;

        int64 tmp_i64 = 0;
        double tmp_dbl = 0;

        for(int i = 2; i < args.length; i++)
        {
            switch(args[i])
            {
                case "-s":
                case "--static":
                    i++;
                    if(i < args.length) this.static_fn = args[i];
                    break;
                case "-m":
                case "--moving":
                    i++;
                    if(i < args.length) this.moving_fn = args[i];
                    break;
                case "-i":
                case "--max-iters":
                    i++;
                    if(i < args.length) if(int64.try_parse(args[i], out tmp_i64)) this.max_iters = (int)tmp_i64;
                    break;
                case "-t":
                case "--tolerance":
                    i++;
                    if(i < args.length) if(double.try_parse(args[i], out tmp_dbl)) this.tolerance = (float)tmp_dbl;
                    break;
                case "-h":
                case "--help":
                    Params.show_help(this, args[0]);
                    exit(0);
                    break;
                case "-o":
                case "--output":
                    i++;
                    if(i < args.length) this.output_fn = args[i];
                    break;
                case "-v":
                case "--vectors":
                    i++;
                    if(i < args.length) this.outvec_fn = args[i];
                    break;
                case "-a":
                case "--landmarks-a":
                    i++;
                    if(i < args.length) this.landmarks_a = args[i];
                    break;
                case "-b":
                case "--landmarks-b":
                    i++;
                    if(i < args.length) this.landmarks_b = args[i];
                    break;
                default:
                    if(this.xfm != null)
                    {
                        int last_i = i;
                        if(!this.xfm.handle_arg(args, ref i))
                        {
                            log_fubar("Failed to handle arg[%d] = \"%s\"\n", last_i, args[last_i]);
                        }
                    }
                    else
                    {
                        log_fubar("Failed to handle arg[%d] = \"%s\"\n", i, args[i]);
                    }
                    break;
            }
        }
    }

    public static void show_help(Params? p, string name)
    {
        stdout.printf("\tUsage: %s [xform]\n", name);
        stdout.printf("\t\t-s --static       [file]   Set the path to the static image\n");
        stdout.printf("\t\t-m --moving       [file]   Set the path tothe moving image\n");
        stdout.printf("\t\t-r --regions      [x,y,z]  Set the number of regions to use in each direction\n");
        stdout.printf("\t\t-i --iters        [int]    Set the maximum number of iterations to perform\n");
        stdout.printf("\t\t-t --tolerance    [float]  Set the error tolerance\n");
        stdout.printf("\t\t-a --landmarks-a  [file]   Set the path to the landmarks for image a.\n");
        stdout.printf("\t\t-b --landmarks-b  [file]   Set the path to the landmarks for image b.\n");
        stdout.printf("\t\t-h --help                  Show the help dialog, then exit\n");

        if(p != null)
        {
            if(p.xfm != null)
            {
                stdout.printf("\t%s\n", p.xfm.method_help().replace("\n", "\n\t"));
            }
            else
            {
                log_warn("No xform method has been registered in the params struct yet.\n");
            }
        }
        else
        {
            log_warn("No parameters to call.\n");
        }
    }
}
