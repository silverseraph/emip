## emip - Experimental Medical Image Processing

### Summary

emip is a program that is used for experimenting with different medical image processing and registartion algorithms across different hardware types.  The frontend of emip is built in vala.  This means that while vala code is used to run the control logic, other languages like C, C++, and CUDA can be added in the final compilation steps.  

emip is built as part of a MS thesis.

### Features

1. Image Registration Techniques
    * Bspline
    * Thin Plate Spline
    * Frequency-based Spline
    * Heirarchial Bspline
2. Hardware variations
    * Single threaded
    * Multithreaded (OpenMP)
    * Multithreaded (Xeon Phi)
    * CUDA
3. Dynamic module loading - Allows for new image registration techniques to be added by placing an appropriate shared object file in the correct directory.  

-------------------------------------------

*Dance like nobody's watching*    
*Sing like no one's listening*    
*Code like nobody's reviewing*    
*git push -f master* 
